import React from 'react';
import { Select, Tooltip } from 'antd';

const { Option } = Select;

const CustomSelect = ({
    handleChange,
    value,
    options,
    disabled,
    label,
    placeholder,
    allowClear,
    tooltipTitle = '',
    defaultValue,
}) => {
    const select = () => (
        <div className="form-item">
            <label htmlFor="">{label}</label>
            <Select
                allowClear={allowClear}
                value={value}
                placeholder={placeholder}
                onChange={handleChange}
                disabled={disabled}
                defaultValue={defaultValue}
                // defaultValue={'Обрати'}
                // unselectable="off"
            >
                {options
                    ? options.map(({ id, name, disabled }) => (
                          <Option
                              key={id || name}
                              value={id || name}
                              disabled={disabled}
                          >
                              {name}
                          </Option>
                      ))
                    : []}
            </Select>
        </div>
    );

    return !disabled ? (
        select()
    ) : (
        <Tooltip title={tooltipTitle}>{select()}</Tooltip>
    );
};

export default CustomSelect;

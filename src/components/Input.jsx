import React from 'react';

const Input = ({
    label,
    handleChange,
    value,
    name,
    type,
    disabled,
    className,
    placeholder,
    step,
    min,
    notify,
    labelOnclick,
}) => {
    return (
        <div className={`form-item ${className ? className : ''}`}>
            <label onClick={labelOnclick ? labelOnclick : null}>{label}</label>
            <input
                min={min}
                step={step}
                disabled={disabled}
                type={type || 'text'}
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={handleChange}
            />
            {notify && <div>{notify}</div>}
        </div>
    );
};

export default Input;

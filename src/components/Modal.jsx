import React from 'react';
import { Modal } from 'antd';
import { connect } from 'react-redux';
import { toogleModal } from '../store/actions/modal';
import DialogWindowHandler from '../containers/UserSide/Exchange/dialogWindows/DialogWindowsHandler';
import { putCurrentChoosedPoduct } from '../store/actions/Users/ordersActionsCreators';
import { putTwoFactor } from '../store/actions/user';

const MyModal = ({ toogleModal, modal, initiateTwoFactorAuth, user }) => {
    // const handleCloseTwoFactor = () => {
    //     initiateTwoFactorAuth({
    //         twoFactorAuthEnabled: false,
    //         image: '',
    //         email: user.email,
    //     });
    //     toogleModal({ type: '', isOpen: false });
    // };
    const handleCloseModal = () => {
        toogleModal({ type: modal.type, isOpen: false, data: null });
    };

    return (
        <Modal
            wrapClassName={
                modal.type === 'show-balance' ? 'balance-modal' : null
            }
            footer={null}
            visible={modal.isOpen}
            onCancel={() => {
                if (modal.type === 'two-factor') {
                    // handleCloseTwoFactor();
                } else {
                    handleCloseModal();
                }
            }}
        >
            <DialogWindowHandler
                isOpen={modal.isOpen}
                modalState={modal.type}
                data={modal.data}
                // handleCloseTwoFactor={handleCloseTwoFactor}
                handleCloseModal={handleCloseModal}
            />
        </Modal>
    );
};

const mapStateToProps = state => ({
    modal: state.modal,
    user: state.user,
});

const mapDispatchToProps = dispatch => ({
    toogleModal: data => dispatch(toogleModal(data)),
    putCurrentChoosedPoduct: val => dispatch(putCurrentChoosedPoduct(val)),
    initiateTwoFactorAuth: payload => dispatch(putTwoFactor(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyModal);

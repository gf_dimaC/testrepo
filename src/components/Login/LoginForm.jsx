import React  from 'react';
import { withFormik } from 'formik';
import { LoginSchema } from '../../schema/user';
import { notification } from 'antd';
import { Form  } from 'formik';
import { NavLink } from 'react-router-dom';
import Input from '../Input';
import Recaptch from '../Recaptcha';

const LoginForm = ({ validateForm, setFieldValue, loading, handleChange }) => {
    const validate = e => {
        if (e.email || e.password) {
            notification.error({
                message: e.email || e.password,
            });
        }
    };
    return (
        <Form>
            <Input
                label="Ваш Email"
                type="email"
                name="email"
                handleChange={handleChange}
                placeholder="Наприклад: example@gmail.com"
            />
            <Input
                label="Ваш пароль"
                type="password"
                name="password"
                handleChange={handleChange}
                placeholder="*******"
            />

            <div className="go-to-reset-pas">
                <NavLink to="/reset_password">Забули пароль?</NavLink>
            </div>
            <Recaptch verifyCallback={e => setFieldValue('captch', true)} />

            <button
                type="submit"
                onClick={() => validateForm().then(validate)}
                className="btn authentication-action-btn"
                disabled={loading}
            >
                Увійти
            </button>
            <div className="go-to-login">
                <NavLink to="/registration">
                    У мене ще немає облікового запису
                </NavLink>
            </div>
        </Form>
    );
};

export default withFormik({
    mapPropsToValues: () => ({
        email: '',
        password: '',
        captch: false,
    }),
    handleSubmit: (values, { props: { login, history } }) => {
        console.log(values, history);
        if (!values.captch) {
            notification.error({
                message: 'Заповніть Recaptchu!',
            });
            return;
        }

        login({ email: values.email, password: values.password }, history);
    },

    validationSchema: LoginSchema,
    displayName: 'LoginForm',
})(LoginForm);

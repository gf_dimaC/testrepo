import React from 'react';
import { NavLink } from 'react-router-dom';
import { slide as Menu } from 'react-burger-menu';
import logo from '../../assets/img/logo_head.svg';
import GuestHeader from './GuestHeader';
import UserHeader from './UserHeader';
import '../../styles/header.scss';
import { connect } from 'react-redux';

const Header = ({ isAuthorized }) => {
    return (
        <header>
            <div className="size-container header">
                <NavLink to="/" className="logo">
                    <div className="logo-name">Biofuel exchange</div>
                    <img src={logo} alt="" />
                </NavLink>

                <div className="mobile-menu">
                    <Menu isOpen={false} right>
                        {localStorage.getItem('TOKEN') ? (
                            <UserHeader />
                        ) : (
                            <GuestHeader />
                        )}
                    </Menu>
                </div>
                <div className="desktop-menu">
                    {localStorage.getItem('TOKEN') ? (
                        <UserHeader />
                    ) : (
                        <GuestHeader />
                    )}
                </div>
            </div>
        </header>
    );
};

const mapStateToProps = state => ({ isAuthorized: state.user.authorized });

export default connect(mapStateToProps)(Header);

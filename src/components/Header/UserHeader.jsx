import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import { Icon } from 'antd';

const UserHeader = () => {
    return (
        <Fragment>
            <NavLink to="/exchange" className="exchange-link">
                Перейти до біржі
            </NavLink>

            <nav>
                <NavLink to="/contracts" activeClassName="active-link">
                    Угоди
                </NavLink>
                <NavLink to="/balance" activeClassName="active-link">
                    Баланс
                </NavLink>
                {/*<NavLink to="/accounts" activeClassName="active-link">*/}
                {/*    Баланс*/}
                {/*</NavLink>*/}
                <NavLink activeClassName="active-link" to="/orders">
                    Замовлення
                </NavLink>

                <NavLink
                    className="profile-link"
                    activeClassName="active-link"
                    to="/profile"
                >
                    <Icon type="user" />
                    Профіль
                </NavLink>
            </nav>
        </Fragment>
    );
};
export default UserHeader;

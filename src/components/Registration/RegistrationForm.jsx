import React, { useEffect, useRef, useState } from 'react';
import { RegistrationSchema } from '../../schema/user';
import { withFormik, Form } from 'formik';
import CustomSelect from '../CustomSelect';
import Input from '../Input';
import { Switch, notification } from 'antd';
import Recaptcha from 'react-recaptcha';
import { NavLink } from 'react-router-dom';
import api from '../../sevices/mixed';
import roles from '../../constants/userRoles';

const RegistrationForm = ({ validateForm, setFieldValue, values, loading }) => {
    const [contries, setCountry] = useState([]);
    const captchaRef = useRef(null);

    const getCountries = async () => {
        try {
            const countries = await api.getAllCountries();
            if (countries.status === 200) {
                setCountry(countries.data);
            }
            console.log(countries);
        } catch (err) {
            console.log(err);
        }
    };
    useEffect(() => {
        captchaRef.current.reset();
        // getCountries();
    }, []);

    const validate = e => {
        console.log('validating', e);
        Object.values(e).forEach(err => {
            notification.warning({
                message: err,
            });
        });
    };

    return (
        <Form>
            <Input
                handleChange={e => setFieldValue('username', e.target.value)}
                label="Ваше ім'я"
                placeholder="Ваше ім'я"
                name="username"
            />
            <CustomSelect
                label="Роль користувача"
                handleChange={value => setFieldValue('userRole', value)}
                placeholder="Виберіть роль"
                options={roles}
                value={values.userRole}
            />
            {/* <CustomSelect
                label="Країна"
                handleChange={countr => setFieldValue('country', countr)}
                placeholder="Виберіть роль"
                options={contries}
                value={values.country}
            /> */}
            <Input
                label="Email"
                type="email"
                name="email"
                handleChange={e => setFieldValue('email', e.target.value)}
                placeholder="Наприклад: example@gmail.com"
            />
            <Input
                label="Пароль"
                type="password"
                name="password"
                handleChange={e => setFieldValue('password', e.target.value)}
                placeholder="******"
            />
            <Input
                label="Підтвердження пароля"
                type="password"
                name="repeatPassword"
                handleChange={e =>
                    setFieldValue('repeatPassword', e.target.value)
                }
                placeholder="******"
            />
            <Input
                label="Телефон"
                type="phone"
                name="phone"
                handleChange={e => setFieldValue('phone', e.target.value)}
                placeholder="Наприклад: +380982992323 або 80982923232"
            />

            {/* <div
                className="form-item"
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >
                <Switch
                    // defaultChecked={showTwoFactor}
                    onChange={val => setFieldValue('onChangeTwoFactor', val)}
                    size="small"
                />
                <span style={{ marginLeft: 30 }}>
                    Увімкнути 2-ух факторну ідентифікацію?
                </span>
            </div> */}
            <div className="confirm">
                <Input
                    label="Я приймаю умови конфіденційності"
                    className="checkbox"
                    labelOnclick={() => window.open('/support')}
                    type="checkbox"
                    name="permission"
                    handleChange={e =>
                        setFieldValue('permission', e.target.checked)
                    }
                />

                <Recaptcha
                    ref={captchaRef}
                    sitekey="6LeHnuMUAAAAAPsx_PucUJkJ0LIzo3esvyocC6SR"
                    render="explicit"
                    verifyCallback={e => setFieldValue('captch', true)}
                />
            </div>

            <button
                onClick={() => validateForm().then(validate)}
                type="submit"
                className="btn authentication-action-btn"
                disabled={loading}
            >
                Зарeєструватися
            </button>

            <div className="go-to-login">
                <NavLink to="/login">Я вже зареєстрованний</NavLink>
            </div>
        </Form>
    );
};

export default withFormik({
    mapPropsToValues: () => ({
        email: '',
        password: '',
        repeatPassword: '',
        username: '',
        permission: false,
        captch: true,
        userRole: 'BS',
        phone: '',
        onChangeTwoFactor: false,
        // country: '',
    }),
    handleSubmit: (values, { props: { registration, history } }) => {
        // console.log(values);
        if (values.password !== values.repeatPassword) {
            notification.warning({
                message: 'Паролі не співпадають!',
            });
            return;
        }
        registration(
            {
                email: values.email,
                firstName: values.username,
                confirmPassword: values.repeatPassword,
                password: values.password,
                phone: values.phone,
                twoFactorAuthEnabled: values.onChangeTwoFactor,
                userRole: values.userRole,
                // userCountry: values.country,
            },
            history,
        );
    },

    validationSchema: RegistrationSchema,
    displayName: 'RegistrationForm',
})(RegistrationForm);

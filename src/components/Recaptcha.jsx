import React, { useRef, useEffect } from 'react';
import Recaptcha from 'react-recaptcha';

const Recaptch = ({ verifyCallback, className }) => {
    const captchaRef = useRef(null);
    useEffect(() => {
        captchaRef.current.reset();
    }, []);
    return (
        <div className={`recaptcha ${className || ''}`}>
            <Recaptcha
                ref={captchaRef}
                sitekey="6LeHnuMUAAAAAPsx_PucUJkJ0LIzo3esvyocC6SR"
                render="explicit"
                // onloadCallback={this.onLoadRecaptcha}
                verifyCallback={verifyCallback}
            />
        </div>
    );
};

export default Recaptch;

import React from 'react';
import Routes from './routing/routes';

import MyModal from './components/Modal';
import './App.scss';
import 'antd/dist/antd.css';

const App = () => {
    return (
        <div className="App">
            <Routes />
            <MyModal />
        </div>
    );
};

export default App;

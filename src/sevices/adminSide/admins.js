// import request from "./API";
import request from '../API';
import { ADMINS } from '../../constants/APIURLS';

export default {
    getAllAdmins: (page = 1) => request('get', ADMINS + `?page=${page}`),
    createAdmin: data => request('post', ADMINS, data),
    updateAdmin: (id, data) => request('patch', ADMINS + `${id}/`, data),
    deleteAdmin: id => request('delete', ADMINS + `${id}/`),
    // getByFilter: (query) => request("get", ALL_USERS + `?${query}`)
};

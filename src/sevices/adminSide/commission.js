import request from '../API';
import { COMMISSIONS, UPDATE_COMMISSIONS } from '../../constants/APIURLS';

export default {
    getCommissions: newQuery => request('get', COMMISSIONS + newQuery),
    updateCommission: data => request('patch', UPDATE_COMMISSIONS, data),
};

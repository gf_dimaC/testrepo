import request from '../API';
import {
    ALL_PRODUCTS,
    UNITS,
    DELIVERY,
    COMMISSION,
    CREATE_RATING,
    ORDER_HISTORY,
    TRANSACTION_HISTORY,
    INVOICE,
    DOWNLOAD_CONTRACT,
    ALL_HISTORY,
} from '../../constants/APIURLS';

export default {
    getAllFProducts: () => request('get', ALL_PRODUCTS),
    getUnits: id => request('get', UNITS + `${id}/`),
    getDelivery: () => request('get', DELIVERY),
    getCommission: () => request('get', COMMISSION),
    getRating: id => request('get', `orders/${id}/questions/`),
    createRatingToOrder: data => request('post', CREATE_RATING, data),
    getOrdersHistory: () => request('get', ORDER_HISTORY),
    getTransactionHistory: () => request('get', TRANSACTION_HISTORY),
    getOpenOrdersHistory: () => request('get', ALL_HISTORY),
    transactionBalanceInvoice: data => request('post', INVOICE, data),
    downloadSertificate: id =>
        request('get', `orders/${id}/download_certificate/`),
    downloadContract: () => request('get', DOWNLOAD_CONTRACT),
    registrationConfirmation: (url) => request('get', 'users/activate/' + url)
};

import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { BASE_URL,REFRESH_TOKEN } from '../constants/APIURLS';

const http = async (method, url, data, type) => {
    const tokenString = localStorage.getItem('ADMINTOKEN')
        ? 'ADMINTOKEN'
        : 'TOKEN';
    let token = localStorage.getItem(tokenString) ? JSON.parse(localStorage.getItem(tokenString)) : null;
    if (token) {
        const parseToken = jwtDecode(token);
        const dateNow = Date.now();
        const difference = parseToken.exp * 1000 - dateNow;
        if (difference <= 0) {
            const refresh_token = localStorage.getItem('TOKENREFRESH');
            const { data } = await axios.post(`${BASE_URL}${REFRESH_TOKEN}`, {
                refresh: refresh_token,
            });
            localStorage.setItem(tokenString, data.access);
            localStorage.setItem('TOKENREFRESH', data.refresh);
            token = data.access;
        }
    }
    // let token = localStorage.getItem('TOKEN')
    //     ? JSON.parse(localStorage.getItem('TOKEN'))
    //     : JSON.parse(localStorage.getItem('ADMINTOKEN'));


    return axios({
        method: method,
        url: BASE_URL + url,
        data: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            Authorization: token ? `Bearer ${token}` : null,
        },
    });
};

export default http;

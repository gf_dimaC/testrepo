export default [
    {
        id: 'S',
        name: 'Продавець',
    },
    {
        id: 'B',
        name: 'Покупець',
    },
    {
        id: 'BS',
        name: 'Продавець-покупець',
    },
];

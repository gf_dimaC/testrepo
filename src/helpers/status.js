export const statuses = [
    {
        id: 'O',
        name: 'Не підтверджений',
    },
    {
        id: 'A',
        name: 'Підтверджений',
    },
    {
        id: 'WP',
        name: 'Очікує оплати',
    },
    {
        id: 'WS',
        name: 'Очікує відвантаження',
    },
    {
        id: 'C',
        name: 'Виконаний',
    },
];

export const deliveryService = (currentChoosedOrder, delivery) => {
    if (!currentChoosedOrder.distance) {
        return 'Не вираховано';
    }

    const volume = currentChoosedOrder.applicationData.purchaseVolume;
    const minValues = currentChoosedOrder.applicationData.units.minVolume;
    const minVolume = minValues
        .map(el => +el)
        .sort()
        .map(el => volume / el);

    const carsCount = Math.ceil(Math.min(...minVolume));

    const distance = +currentChoosedOrder.distance.value / 1000;
    const pricePerKm = +delivery[0].price;
    const minPrice = +delivery[0].minPrice;
    const deliverySum = pricePerKm * distance;
    return minPrice > deliverySum
        ? minPrice * carsCount
        : deliverySum * carsCount;
};

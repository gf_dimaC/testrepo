import _ from 'lodash';

export const removeNulls = obj => {
    return Object.keys(obj).forEach(key => {
        if (obj[key] === null) {
            delete obj[key];
        } else if (_.isObject(obj[key])) {
            Object.keys(obj[key]).forEach(key2 => {
                if (obj[key][key2] === null) {
                    delete obj[key][key2];
                }
            });
        }
    });
};
export const productsRestructuring = arr => {
    return arr.map(el1 => {
        return {
            description: el1.description,
            id: el1.id,
            name: el1.name,
            children: el1.rawMaterials.map(el2 => {
                return {
                    description: el2.description,
                    id: el2.id,
                    name: el2.name,
                    children: el2.categories.map(el => ({
                        description: el.description,
                        id: el.id,
                        name: el.name,
                        children: el.categories,
                    })),
                };
            }),
        };
    });
};
export const sliceFalseValuesFromList = data => {
    return Object.entries(data).reduce((acc, el) => {
        if (el[1] || (el[1] === false && el)) {
            acc[el[0]] = el[1];
        }
        return acc;
    }, {});
};

export const validateWarehouse = warehouseData =>
    warehouseData.stockBuilding.trim() &&
    warehouseData.stockRegion.trim() &&
    warehouseData.stockCity.trim() &&
    warehouseData.stockCountry.trim() &&
    warehouseData.stockRegion.trim() &&
    warehouseData.stockStreet.trim()
        ? false
        : true;

export const transformIncomeRatingtoStateStracture = rating =>
    rating.reduce((acc, rat) => {
        acc[rat.id] = 0;
        return acc;
    }, {});

export const statusColors = {
    відкритий: 'blue',
    активний: '#7ec365;',
    закритий: '#c81414',
    скасований: 'red',
};

export const makeQueryFromState = state => {
    let res = {};
    Object.keys(state).forEach(el => {
        if (typeof state[el] === 'string' || typeof state[el] === 'number') {
            res[el] = `${el}=${state[el]}`;
        } else if (
            state[el] !== null &&
            state[el] !== undefined &&
            Object.keys(state[el]).length > 0
        ) {
            const innerKeys = Object.keys(state[el]);
            res[el] = `${innerKeys[0]}=${state[el][innerKeys[0]]}&${
                innerKeys[1]
            }=${state[el][innerKeys[1]]}`;
        } else {
            res[el] = ``;
        }
    });

    return Object.values(res)
        .filter(el => el)
        .join('&');
};

export const getCategories = arr => {
    let resa = [];
    const res = arr => {
        arr.forEach(el => {
            el.children ? res(el.children) : resa.push(el);
        });
    };
    res(arr);
    return [...new Set(resa.map(s => s.id))].map(id => {
        const result = resa.find(el => el.id === id);
        return {
            id,
            ...result,
        };
    });
};

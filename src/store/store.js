import { createStore, applyMiddleware } from 'redux';
import RootReducer from './reducers/ROOT';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

const middleware = composeWithDevTools(applyMiddleware(ReduxThunk));

const store = createStore(RootReducer, middleware);

export default store;

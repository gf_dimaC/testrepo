import { PUT_SOCKET, REMOVE_SOCKET } from '../../constants/constants';

const initalState = null;

const orderReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_SOCKET:
            return payload;
        case REMOVE_SOCKET:
            return initalState;
        default:
            return state;
    }
};

export default orderReducer;

import {
    PUT_BUY_ORDERS,
    PUT_SELL_ORDERS,
    PUT_FILTERED_OREDERS,
    PUT_CREATED_BUY_ORDER,
    PUT_CREATED_SELL_ORDER,
} from '../../constants/constants';

const initalState = {
    buyOrders: [],
    sellOrders: [],
    error: null,
};

const orderReducer = (state = initalState, { type, payload }) => {
    // console.log(payload);
    switch (type) {
        case PUT_BUY_ORDERS:
            return { ...state, buyOrders: payload, error: null };
        case PUT_SELL_ORDERS:
            return { ...state, sellOrders: payload, error: null };
        case PUT_FILTERED_OREDERS:
            return { ...payload, error: null };
        case PUT_CREATED_BUY_ORDER:
            return {
                ...state,
                buyOrders: [payload, ...state.buyOrders],
                error: null,
            };
        case PUT_CREATED_SELL_ORDER:
            return {
                ...state,
                sellOrders: [payload, ...state.sellOrders],
                error: null,
            };

        default:
            return state;
    }
};

export default orderReducer;

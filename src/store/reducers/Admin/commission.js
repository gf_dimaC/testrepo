import { PUT_COMMISSIONS } from '../../../constants/constants';

const initalState = {
    count: 0,
    data: [],
};

const adminCommissionReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_COMMISSIONS:
            return payload;

        default:
            return state;
    }
};

export default adminCommissionReducer;

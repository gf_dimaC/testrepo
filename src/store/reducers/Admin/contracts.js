import {
    PUT_CONTRACTS,
    PUT_CONTRACTS_STATISTIC,
    PUT_INITIAL_CONTRACT,
} from '../../../constants/constants';

const initalState = {
    allContracts: {
        data: [],
        count: 0,
    },
    contractsStatistics: [],
    initialContract: null,
};

const contractReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_CONTRACTS:
            return { ...state, allContracts: payload };
        case PUT_CONTRACTS_STATISTIC:
            return { ...state, contractsStatistics: payload };
        case PUT_INITIAL_CONTRACT:
            return { ...state, initialContract: payload };
        default:
            return state;
    }
};

export default contractReducer;

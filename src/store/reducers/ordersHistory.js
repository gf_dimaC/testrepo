import {
    PUT_SUCCESS_CLOSED_HISTORY,
    PUT_TRANSACTION_HISTORY,
    PUT_ALL_HISTORY,
} from '../../constants/constants';

const initalState = {
    transactionHistory: [],
    successClosedHistory: [],
    allHistory: [],
};

const ordersHistoryReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_SUCCESS_CLOSED_HISTORY:
            return { ...state, successClosedHistory: payload };
        case PUT_TRANSACTION_HISTORY:
            return { ...state, transactionHistory: payload };
        case PUT_ALL_HISTORY:
            return { ...state, allHistory: payload };
        default:
            return state;
    }
};

export default ordersHistoryReducer;

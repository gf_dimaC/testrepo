import {
    PUT_CONTRACTS,
    PUT_CONTRACTS_STATISTIC,
    PUT_INITIAL_CONTRACT,
} from '../../../constants/constants';
import { onChangeFilters } from './filters';
import api from '../../../sevices/adminSide/contracts';
import { notification } from 'antd';
import { makeQueryFromState } from '../../../helpers/functions';

const putContracts = payload => ({ type: PUT_CONTRACTS, payload });
const putContractsStatistics = payload => ({
    type: PUT_CONTRACTS_STATISTIC,
    payload,
});
const putInitialContract = payload => ({ type: PUT_INITIAL_CONTRACT, payload });

export const fetchContracts = page => async (dispatch, getState) => {
    try {
        page && dispatch(onChangeFilters('page', page));
        const filters = getState().filtersReducer;

        const quer =
            Object.keys(filters).length > 0
                ? makeQueryFromState(filters)
                : undefined;
        const newQuery = quer !== undefined && quer ? `?${quer}` : '';
        console.log('now we are shaking here', newQuery);
        const { data } = await api.getContracts(newQuery);
        dispatch(putContracts({ data: data.results, count: data.count }));
    } catch (e) {
        console.log(e);
    }
};

export const fetchContractsStatistics = () => async dispatch => {
    try {
        const { data } = await api.getContractsStatistic();
        dispatch(putContractsStatistics([data]));
    } catch (e) {
        console.log(e);
    }
};

export const fetchInitialContract = id => async dispatch => {
    try {
        const { data } = await api.getInitialContract(id);
        dispatch(putInitialContract(data));
    } catch (e) {
        console.log(e);
    }
};

export const updateContract = (id, updatingData) => async dispatch => {
    try {
        console.log(id, updatingData);
        const { data } = await api.updateContract(id, updatingData);
        notification.success({
            message: 'Угоду оновленно!',
        });
    } catch (e) {
        console.log(e);
        notification.success({
            message: 'Помилка сервера!',
        });
    }
};

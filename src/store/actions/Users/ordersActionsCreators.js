import {
    PUT_BUY_ORDERS,
    PUT_SELL_ORDERS,
    PUT_FILTERED_OREDERS,
    PUT_SOCKET,
    PUT_CREATED_BUY_ORDER,
    PUT_CREATED_SELL_ORDER,
    PUT_OPEN_OREDER,
    PUT_ACTIVE_ORDER,
    PUT_ONE_OPEN_ORDER_WHEN_CREATED,
    PUT_CHOOSED_PRODUCT,
    PUT_ONE_ACTIVE_ORDER_WHEN_CREATED,
    DELETE_FROM_ACTIVE,
    DELETE_FROM_OPENED,
    REMOVE_SOCKET,
} from '../../../constants/constants';

export const putFilteredOrders = payload => ({
    type: PUT_FILTERED_OREDERS,
    payload,
});

export const putBuyOrders = payload => ({
    type: PUT_BUY_ORDERS,
    payload,
});
export const putSellOrders = payload => ({
    type: PUT_SELL_ORDERS,
    payload,
});

export const putSocket = payload => ({
    type: PUT_SOCKET,
    payload,
});

export const onOpenOrders = payload => ({
    type: PUT_OPEN_OREDER,
    payload,
});
export const onActiveOrders = payload => ({
    type: PUT_ACTIVE_ORDER,
    payload,
});
export const onOpenOrdersCreate = payload => ({
    type: PUT_ONE_OPEN_ORDER_WHEN_CREATED,
    payload,
});
export const onActiveOrdersCreate = payload => ({
    type: PUT_ONE_ACTIVE_ORDER_WHEN_CREATED,
    payload,
});
export const onCreateBuyOrder = payload => ({
    type: PUT_CREATED_BUY_ORDER,
    payload,
});
export const onCreateSellOrder = payload => ({
    type: PUT_CREATED_SELL_ORDER,
    payload,
});
export const putCurrentChoosedPoduct = payload => ({
    type: PUT_CHOOSED_PRODUCT,
    payload,
});

export const onActiveOrdersDeleted = payload => ({
    type: DELETE_FROM_ACTIVE,
    payload,
});
export const onOpenOrdersDeleted = payload => ({
    type: DELETE_FROM_OPENED,
    payload,
});

export const removeSocket = () => ({ type: REMOVE_SOCKET });

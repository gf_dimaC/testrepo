import { notification } from 'antd';
import {
    onCreateSellOrder,
    putFilteredOrders,
    onCreateBuyOrder,
    onOpenOrdersDeleted,
    onActiveOrdersDeleted,
    onActiveOrdersCreate,
    putBuyOrders,
    putSellOrders,
    onActiveOrders,
    putSocket,
    onOpenOrders,
    onOpenOrdersCreate,
} from './ordersActionsCreators';
import { toogleModal } from '../modal';
import { tooglePreloader } from '../preloader';

const onDepthsDistanceHandler = data => (dispatch, getState) => {
    const buyOrders = getState().orders.buyOrders;
    const sellOrders = getState().orders.sellOrders;
    if (data.depth === '1') {
        const distances = data.distances;
        const newArray = buyOrders.map(order => ({
            ...order,
            distance: distances[order.id]
                ? distances[order.id]
                : order.distance,
        }));
        dispatch(putBuyOrders(newArray));
    } else {
        const distances = data.distances;
        const newArray = sellOrders.map(order => ({
            ...order,
            distance: distances[order.id]
                ? distances[order.id]
                : order.distance,
        }));
        dispatch(putSellOrders(newArray));
    }
};

export const SocketHandler = (message, ws) => {
    return (dispatch, getState) => {
        const socket = getState().socket;
        const data = JSON.parse(message.data);
        if (!socket) {
            dispatch(putSocket(ws));
        }
        console.log('Socket Data', data);
        switch (data.type2) {
            case 'best_deal':
                if (data.depth === '1') {
                    dispatch(putBuyOrders(data.messageHistory));
                } else {
                    dispatch(putSellOrders(data.messageHistory));
                }
                break;
            case 'distance':
                dispatch(onDepthsDistanceHandler(data));
                break;
            case 'active':
                dispatch(onActiveOrders(data.activeOrders));
                break;
            case 'open':
                dispatch(onOpenOrders(data.openOrders));
                break;
            case 'open_created':
                dispatch(onOpenOrdersCreate(data.openOrder));
                dispatch(toogleModal({ type: '', isOpen: false }));
                // setModal({ ...modalState, visible: false });
                break;
            case 'active_created':
                dispatch(onActiveOrdersCreate(data.activeOrder));
                // setModal({ ...modalState, visible: false });
                break;
            case 'active_deleted':
                dispatch(onActiveOrdersDeleted(data.id));

                break;
            case 'open_deleted':
                dispatch(onOpenOrdersDeleted(data.id));

                break;
            case 'notification':
                notification.warning({
                    message: data.message,
                });
                dispatch(tooglePreloader(false));
                dispatch(toogleModal({ type: '', isOpen: false }));
                break;
            case 'leave':
                break;
            case 'created':
                if (data.depth === '1') {
                    dispatch(onCreateBuyOrder(data.message));
                    // dispatch(toogleModal({ type: '', isOpen: false }));
                    // setModal({ ...modalState, visible: false });
                } else if (data.depth === '2') {
                    dispatch(onCreateSellOrder(data.message));
                    // dispatch(toogleModal({ type: '', isOpen: false }));
                    // setModal({ ...modalState, visible: false });
                }
                break;
            case 'filter':
                dispatch(
                    putFilteredOrders({
                        buyOrders: data.filterBuyOrder,
                        sellOrders: data.filterSellOrder,
                    }),
                );
                break;
            case 'join':
                if (data.join === '1') {
                    dispatch(putBuyOrders(data.messageHistory));
                } else {
                    dispatch(putSellOrders(data.messageHistory));
                }
                break;
            case 'error':
                notification.error({
                    message: data.error,
                });
                dispatch(tooglePreloader(false));
                break;
            default:
                break;
        }
    };
};

export const SocketErrorHandler = err => {
    return dispatch => {
        console.log('Socket Error Handler', err);
    };
};

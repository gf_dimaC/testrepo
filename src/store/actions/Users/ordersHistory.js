import api from '../../../sevices/exchange/index';
import {
    PUT_SUCCESS_CLOSED_HISTORY,
    PUT_TRANSACTION_HISTORY,
    PUT_ALL_HISTORY,
} from '../../../constants/constants';

const putOrderHistoryData = payload => ({
    type: PUT_SUCCESS_CLOSED_HISTORY,
    payload,
});
const putTransactionsHistoryData = payload => ({
    type: PUT_TRANSACTION_HISTORY,
    payload,
});

const putAllHistoryDta = payload => ({ type: PUT_ALL_HISTORY, payload });

export const getOrderHistory = () => {
    return dispatch =>
        api
            .getOrdersHistory()
            .then(({ data: { results } }) =>
                dispatch(putOrderHistoryData(results)),
            )
            .catch(err => console.log(err));
};
const fimc = (a, b) => a * b;
export const getTransactionsHistory = () => {
    return dispatch =>
        api
            .getTransactionHistory()
            .then(({ data }) => {
                dispatch(putTransactionsHistoryData(data));
            })
            .catch(err => console.log(err));
};

export const getAllHistroyORders = () => {
    return dispatch =>
        api
            .getOpenOrdersHistory()
            .then(({ data: { results } }) => {
                dispatch(putAllHistoryDta(results));
            })
            .catch(err => console.log(err));
};

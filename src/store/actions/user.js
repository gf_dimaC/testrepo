import {
    LOGOUT,
    FETCH_USER_DATA,
    PUT_PROFILE_DATA,
    PRELOADER,
    TWO_FACTOR_STEP,
    RESET_USER_MODEL,
} from '../../constants/constants';
import api from '../../sevices/userRequests';
import { verifyAddress } from '../../sevices/exchange/google';
import { notification } from 'antd';
import moment from 'moment';
import 'moment/locale/uk';

const putProfileData = user => ({ type: PUT_PROFILE_DATA, payload: user });

const userPreloader = payload => ({ type: PRELOADER, payload });

const putUserData = payload => ({
    type: FETCH_USER_DATA,
    payload,
});
export const reset = () => ({ type: RESET_USER_MODEL });

export const putTwoFactor = payload => ({ type: TWO_FACTOR_STEP, payload });

export const Logout = () => {
    if (localStorage.getItem('ADMINTOKEN')) {
        localStorage.removeItem('ADMINTOKEN');
        localStorage.removeItem('TOKENREFRESH');
    } else {
        localStorage.removeItem('TOKEN');
        localStorage.removeItem('user');
        localStorage.removeItem('TOKENREFRESH');
    }

    return {
        type: LOGOUT,
    };
};

const fetchUserProfileData = async () =>
    await Promise.all([api.userLoginHistory(1), api.GetUserProfile()]);

export const Registration = (payload, history) => {
    return async dispatch => {
        dispatch(userPreloader(true));
        try {
            const { data } = await api.Registration(payload);
            // if (data.twoFactorAuthEnabled) {
            //     dispatch(
            //         putTwoFactor({
            //             twoFactorAuthEnabled: true,
            //             image: data.imageBase64,
            //             email: data.email,
            //         }),
            //     );
            // } else {
            notification.success({
                message:
                    'Лист з активацією аккаунта відправленно на Ваш email.',
            });
            dispatch(userPreloader(false));
            history.push('/login');
            // }
        } catch (e) {
            console.log('catch', e);
            dispatch(userPreloader(false));
            if (e.response) {
                notification.error({
                    message: e.response.statusText,
                    description: Object.values(e.response.data)[0],
                });
            } else {
                notification.error({
                    message: 'Помилка сервера!',
                });
            }
        }
    };
};

export const getUserProfile = routeHistory => {
    return async dispatch => {
        try {
            const [history, userData] = await fetchUserProfileData();

            const user = {
                ...userData.data,
                authHistory: history.data.results.map(ipHistory => ({
                    ...ipHistory,
                    loginDatetime: moment(ipHistory.loginDatetime)
                        .locale('uk')
                        .format('L'),
                })),
            };
            routeHistory.push('/profile');
            localStorage.setItem('user', JSON.stringify(user));

            dispatch(putUserData(user));



            return userData;
        } catch (e) {
            console.log('Error in user Profile', e);
            // dispatch(putLoginError(e.data.nonFieldErrors[0]));
        }
    };
};

export const Login = (payload, history) => {
    return async dispatch => {
        dispatch(userPreloader(true));
        try {
            const { data } = await api.login(payload);


            if (data.isStaff) {
                localStorage.setItem('ADMINTOKEN', JSON.stringify(data.access));
                history.push('/admin/dashboard');
            } else {
                localStorage.setItem('TOKEN', JSON.stringify(data.access));
                dispatch(getUserProfile(history));
            }
            localStorage.setItem('TOKENREFRESH', JSON.stringify(data.refresh));
        } catch (e) {
            console.log('ERROR in CaTch', e);
            dispatch(userPreloader(false));
            if (e.response) {
                notification.error({
                    message: Object.values(e.response.data)[0],
                    description: 'Спробуйте ще раз!',
                });
            }
        }
    };
};

// export const checkTwoFactor = (userData, history) => {
// //     return async dispatch => {
// //         const res = await api.checkForTwoFactor(userData);
// //         console.log(res);
// //         // if (data.twoFactorEnable) {
// //         //     // dispatch(putTwoFactor(data));
// //         // } else {
// //         //     console.log('false');
// //         // }
// //     };
// // };

export const editUserPRofile = userData => {
    return async (dispatch, getState) => {
        dispatch(userPreloader(true));
        const authHistory = getState().user.authHistory;
        try {
            const { data } = await api.editProfileInfo(userData);
            notification.success({
                message: 'Профіль оновлено!',
            });
            localStorage.setItem('user', JSON.stringify({...data, authHistory}));
            dispatch(putProfileData(data));
        } catch (e) {
            dispatch(userPreloader(false));
            if (e.response) {
                Object.keys(e.response.data).forEach(el => {
                    const innerField = e.response.data[el];
                    if (
                        Array.isArray(innerField) ||
                        typeof innerField === 'string'
                    ) {
                        notification.error({
                            message: innerField || 'Error',
                        });
                    } else if (Object.keys(innerField).length > 0) {
                        Object.keys(innerField).forEach(el2 => {
                            notification.error({
                                message: innerField[el2][0],
                            });
                        });
                    } else {
                        notification.error({
                            message: 'Помилка сервера!',
                        });
                    }
                });
            } else {
                notification.error({
                    message: 'Помилка сервера!',
                });
            }
        }
    };
};

export const verifyWarehouse = wareHouseData => {
    return async (dispatch,getState) => {
        const query =
            wareHouseData.stockStreet +
            ', ' +
            wareHouseData.stockBuilding +
            ', ' +
            `+${wareHouseData.stockCity} + ${wareHouseData.stockCountry ||
                'Украина'}`;

        try {
            const { data } = await verifyAddress(query);

            const [googleresults] = data.results;
            if (data.status === 'OK') {
                const newValues = {
                    // ...values,
                    stock: {
                        ...wareHouseData,
                        stockCoordinates: {
                            latitude: googleresults.geometry.location.lat.toFixed(
                                8,
                            ),
                            longitude: googleresults.geometry.location.lng.toFixed(
                                8,
                            ),
                        },
                        isVerified: true,
                    },
                };

                const { data: updatedData } = await api.editProfileInfo(
                    newValues,
                );
                const authHistory = getState().user.authHistory;
                localStorage.setItem('user', JSON.stringify({...updatedData, authHistory}));
                dispatch(putProfileData(updatedData));

                notification.success({
                    message: 'Склад веріфіковано успішно!!',
                });
            } else {
                notification.error({
                    message: 'Вибачте, такої адреси не знайдено.',
                });
            }
        } catch (e) {
            console.log(e);
        }
    };
};

import React from 'react';

import UnpaidAccounts from './UnpaidAccounts';
import HistoryAccounts from './HistoryAccounts';

import './accounts.scss';
const unPaidBills = [
    {
        id: '24244',
        type: 'Купівля',
        number: '2245367897876',
        date: ' 18:30  12/03/2018',
        sum: 4000,
    },
    {
        id: '24214',
        type: 'Купівля',
        number: '2245367897876',
        date: ' 18:30  12/03/2018',
        sum: 4000,
    },
    {
        id: '24224',
        type: 'Купівля',
        number: '2245367897876',
        date: ' 18:30  12/03/2018',
        sum: 4000,
    },
    {
        id: '121',
        type: 'Купівля',
        number: '2245367897876',
        date: ' 18:30  12/03/2018',
        sum: 4000,
    },
];
const Accounts = () => {
    const unaidBillsTotalSub = unPaidBills.reduce(
        (sum, bill) => sum + bill.sum,
        0,
    );
    return (
        <div className="accounts-page">
            <div className="page-title">Рахунки</div>

            <div className="total-block">
                <div className="unpaid-accounts">
                    <span className="title">Несплачені рахунки</span>
                    <span className="value">{unaidBillsTotalSub} грн</span>
                </div>
                <div className="overdue-accounts">
                    <span className="title">Прострочені рахунки </span>
                    <span className="value">245000 грн</span>
                </div>
            </div>

            <UnpaidAccounts data={unPaidBills} />

            <HistoryAccounts />
        </div>
    );
};

export default Accounts;

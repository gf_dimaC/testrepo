import React, { useEffect, useState } from 'react';
import api from '../../../sevices/adminSide/news';
import moment from 'moment';

const UserNews = () => {
    const [news, setNews] = useState([]);
    useEffect(() => {
        getNews().then(({ data }) => setNews(data.results));
    }, []);
    const getNews = () => api.getNews();
    return (
        <div className="user-news">
            <h1 className="title">Новини біржі</h1>
            {news.map(iNews => (
                <div className="news-container">
                    <div className="date">{`${moment(iNews.createdDate).format(
                        'LT',
                    )} / ${moment(iNews.createdDate).format('L')}`}</div>
                    <div
                        className="content text"
                        dangerouslySetInnerHTML={{ __html: iNews.text }}
                    ></div>
                </div>
            ))}
        </div>
    );
};
export default UserNews;

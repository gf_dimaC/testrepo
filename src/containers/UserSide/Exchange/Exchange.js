import React, { useEffect } from 'react';
import Filters from './Filters';
import ActiveOrders from './ActiveOrders';
import OpenOrders from './OpenOrders';
import TradeHistory from './TradeHistory';
import { connect } from 'react-redux';
import {
    // putCurrentChoosedPoduct,
    removeSocket,
} from '../../../store/actions/Users/ordersActionsCreators';
import {
    SocketHandler,
    SocketErrorHandler,
} from '../../../store/actions/Users/orders';
import { getDelivery } from '../../../store/actions/Users/delivery';
import EchangeTables from './EchangeTables';
import handleSockets from '../../../helpers/sockets';
import './exchange.scss';

const Exchange = ({ socket, SocketHandler, getDelivery, userId }) => {
    useEffect(
        () => {
            getDelivery();
            if (userId && !socket) {
                // console.log('reconnect');
                console.log('token', userId);
                handleSockets(SocketHandler, SocketErrorHandler, userId);
            }
        },
        [userId],
    );
    // console.log('main rendering');
    return (
        <div className="exchange-page">
            <Filters />

            <ActiveOrders />

            <EchangeTables />

            <OpenOrders />

            <TradeHistory />
        </div>
    );
};

const mapStateToProps = state => ({
    socket: state.socket,
    userId: state.user.id,
});
const mapDispatchToProps = dispatch => ({
    SocketHandler: (val, socket) => dispatch(SocketHandler(val, socket)),
    getDelivery: () => dispatch(getDelivery()),
    SocketErrorHandler: err => dispatch(SocketErrorHandler(err)),
    removeSocket: () => dispatch(removeSocket()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Exchange);

import React, { useState, useEffect } from 'react';
import { Table, Icon } from 'antd';
// import columns from '../../../helpers/columns';
import { connect } from 'react-redux';
import { openModal } from '../../../store/actions/modal';

const iconStyle = {
    color: '#53be4f',
    fontSize: '20px',
};

const ActiveOrders = ({ activeOrders = [], showModal, socket }) => {
    const activeOrderColumns = [
        {
            title: 'Тип',
            dataIndex: 'type',
            key: 'type',
            width: 100,
        },
        {
            title: 'Сировина',
            dataIndex: 'productName',
            key: 'category',
            width: 150,
            render: (i, data) =>
                data.applicationData && data.applicationData.product.name,
        },
        {
            title: 'Ціна ',
            dataIndex: 'price',
            key: 'price',
            width: 100,
            render: (a, data) => `${data.price} грн`,
        },
        {
            title: 'Обсяг ',
            dataIndex: 'applicationData.purchaseVolume',
            key: 'amount',
            width: 100,
            render: (name, item) =>
                `${item.applicationData.purchaseVolume} ${
                    item.applicationData.units.shortName
                }`,
        },
        {
            title: 'Всього ',
            dataIndex: 'sum',
            key: 'sum',
            width: 150,
            render: (a, data) => `${data.sum} грн`,
        },
        {
            title: 'Відстань ',
            dataIndex: 'distance',
            key: 'distance',
            width: 150,
            render: (as, data) =>
                data.distance ? (
                    data.distance.text
                ) : (
                    <Icon type="loading" style={iconStyle} />
                ),
        },
        {
            title: 'Доставка ',
            dataIndex: 'deliveryPrice',
            key: 'deliveryPrice',
            width: 150,
            render: (a, data) => `${data.deliveryPrice || 0} грн`,
        },
        {
            title: 'Орієнт. доставка ',
            dataIndex: 'oriented_delivery',
            key: 'oriented_delivery',
            width: 150,
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            width: 150,

            render: (as, data) => (
                <div className="table-action-btn orders-controls">
                    {/*{!data.verified && (*/}
                    <button
                        className="btn btn-blue"
                        disabled={data.verified}
                        onClick={() => showModal('accept-active', data)}
                    >
                        Виконати
                    </button>
                    {/*)}*/}

                    <button
                        className="btn btn-red"
                        disabled={data.verified}
                        onClick={() => declineActiveOrder(data.id)}
                    >
                        Скасувати
                    </button>
                </div>
            ),
        },
    ];
    const [loading, toogleLoader] = useState(true);
    const declineActiveOrder = id => {
        socket.send(
            JSON.stringify({
                command: 'active',
                action: 'unsubscribe',
                message: {
                    order_id: id,
                },
            }),
        );
    };
    useEffect(
        () => {
            if (socket) {
                toogleLoader(false);
            }
        },
        [socket],
    );
    return (
        <div className="active-orders user-table">
            <div className="table-title">Активні угоди</div>

            <Table
                loading={loading}
                scroll={{x: true}}
                columns={activeOrderColumns}
                dataSource={activeOrders}
                pagination={false}
            />
        </div>
    );
};

const mapStateToProps = state => ({
    activeOrders: state.activeOrders.activeOrders,
    socket: state.socket,
});

const mapDispatchToProps = dispatch => ({
    showModal: (type, prod) => dispatch(openModal(type, prod)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(ActiveOrders);

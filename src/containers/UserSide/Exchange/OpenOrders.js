import React, { useState, useEffect } from 'react';
import { Table } from 'antd';
import { useSelector } from 'react-redux';
// import columns from '../../../helpers/columns';
import { connect } from 'react-redux';
import moment from 'moment';

const openStyle = {
    display: 'flex',
};

const OpenOrders = ({ openOrders, socket }) => {
    const userEmail = useSelector(state => state.user.email);
    const declineOpenOrder = id => {
        socket.send(
            JSON.stringify({
                command: 'open',
                action: 'unsubscribe',
                message: {
                    order_id: id,
                },
            }),
        );
    };
    const acceptOpenOrder = id => {
        socket.send(
            JSON.stringify({
                command: 'active',
                action: 'subscribe',
                message: {
                    order_id: id,
                },
            }),
        );
    };
    const openOrdersColumns = [
        {
            title: 'Тип',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'Сировина',
            dataIndex: 'applicationData.product.name',
            key: 'category',
        },
        {
            title: 'Ціна',
            dataIndex: 'price',
            key: 'price',
            render: (a, data) => `${data.price} грн`,
        },
        {
            title: 'Обсяг',
            dataIndex: 'applicationData.purchaseVolume',
            key: 'amount',
            render: (name, item) =>
                `${item.applicationData.purchaseVolume}  ${
                    item.applicationData.units.shortName
                }`,
        },
        {
            title: 'Всього',
            dataIndex: 'sum',
            key: 'total',
            render: (a, data) => `${data.sum} грн`,
        },
        {
            title: 'Дата відкриття',
            dataIndex: 'opened',
            key: 'opened',
            render: (a, data) =>
                `${moment(data.opened).format('LT')} / ${moment(
                    data.opened,
                ).format('L')}`,
        },
        {
            title: 'Зарезервовані кошти',
            dataIndex: 'commission',
            key: 'commission',
            render: (i, data) => data.commission + ' грн',
        },
        // {
        //     title: 'Статус',
        //     dataIndex: 'status',
        //     key: 'status',
        // },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            render: (x, data) => {
                return (
                    <div style={openStyle}>
                        <button
                            className="btn btn-red"
                            onClick={() => declineOpenOrder(data.id)}
                        >
                            Скасувати
                        </button>
                        {data.email === userEmail && (
                            <button
                                className="btn btn-blue"
                                onClick={() => acceptOpenOrder(data.id)}
                            >
                                Прийняти
                            </button>
                        )}
                    </div>
                );
            },
        },
    ];
    const [loading, toogleLoader] = useState(true);

    useEffect(
        () => {
            if (socket) {
                toogleLoader(false);
            }
        },
        [socket],
    );
    return (
        <div className="open-orders">
            <div className="user-table">
                <div className="title-order-type table-title">
                    Відкриті замовлення
                </div>

                <Table
                    loading={loading}
                    dataSource={openOrders}
                    columns={openOrdersColumns}
                    pagination={false}
                />
            </div>
        </div>
    );
};

const mapStatetoProps = state => ({
    openOrders: state.activeOrders.openOrders,
    socket: state.socket,
});

export default connect(mapStatetoProps)(OpenOrders);

import React, { useState, useRef } from 'react';
import { Table, Icon, Switch, notification } from 'antd';
import columns from '../../../helpers/columns';

const OrdersList = ({
    data,
    type,
    onOpenCreateWindow,
    loading,
    user,
    handleBestDeal,
}) => {
    const [sortInfo, setInfo] = useState({});
    const ref = useRef(null);
    const handleChange = (pagination, filters, sorter) => {
        setInfo(sorter);
    };
    const onCreateOrder = () => {
        user.verificationStatus === 'V'
            ? onOpenCreateWindow(type)
            : notification.error({
                  message:
                      'Для створення заказу необхідно веріфікувати ваш аккаунт!',
              });
    };
    const renderCreateBtn = () => {
        if (user.userRole === 'BS') {
            return (
                <button className="btn" onClick={onCreateOrder}>
                    <Icon type="plus-circle" />
                    Створити
                </button>
            );
        } else if (user.userRole === 'B' && type === 'buy') {
            return (
                <button className="btn" onClick={onCreateOrder}>
                    <Icon type="plus-circle" />
                    Створити
                </button>
            );
        } else if (user.userRole === 'S' && type === 'sell') {
            return (
                <button className="btn" onClick={onCreateOrder}>
                    <Icon type="plus-circle" />
                    Створити
                </button>
            );
        } else return null;
    };
    return (
        <div className="orders-list">
            <div className="user-table">
                <div className="title-order-type table-title">
                    <span>{type === 'buy' ? 'Купівля' : 'Продаж'}</span>

                    <div className="switch-block">
                        <label htmlFor="">Краща пропозиція</label>
                        <Switch onChange={val => handleBestDeal(val, type)} />
                    </div>

                    {renderCreateBtn()}
                </div>

                <Table
                    ref={ref}
                    rowClassName={st => (st.isOpen ? 'isOpen' : '')}
                    loading={loading}
                    dataSource={data}
                    scroll={{ x: true }}
                    columns={columns.orderListColumns(type, sortInfo)}
                    pagination={false}
                    onChange={handleChange}
                    onRow={(record, rowIndex) => {
                        return {
                            onClick: () => {
                                onOpenCreateWindow(`${type}-confirm`, record);
                            },
                        };
                    }}
                />
            </div>
        </div>
    );
};

export default OrdersList;

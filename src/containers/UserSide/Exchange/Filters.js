import React, { useState, useEffect } from 'react';
import { Cascader } from 'antd';
import regions from '../../../constants/regions';
import { connect } from 'react-redux';
import { fetchAllProductsData } from '../../../store/actions/productsandfilters';
import api from '../../../sevices/exchange/index';
import CustomSelect from '../../../components/CustomSelect';
import { sliceFalseValuesFromList } from '../../../helpers/functions';

const moneyForm = [
    {
        name: 'З НДС',
        id: '1',
    },
    {
        name: 'БЕЗ НДС',
        id: '0',
    },
];

const Filters = ({ fetchForProducts, products, socket }) => {
    const [raw, setRaw] = useState([]);

    const [units, setUnit] = useState({
        currentUnit: undefined,
        unitsOptions: [],
    });
    const [region, setRegion] = useState('');
    const [pdv, setPdv] = useState('');
    // console.log(region);

    const handleSocket = (filter, clear) => {
        // console.log('asdasdas', socket.readyState);
        if (clear && socket.readyState === 1) {
            socket.send(
                JSON.stringify({
                    command: 'join',
                }),
            );
        } else {
            socket.send(
                JSON.stringify({
                    command: 'filter',
                    message: filter,
                }),
            );
        }
    };

    useEffect(() => {
        if (!products.length) {
            fetchForProducts();
            console.log('HERE');
        }
    }, []);

    useEffect(
        () => {
            const [productId, rawMaterialId, categoryId] = raw;
            const val = {
                productId,
                rawMaterialId,
                categoryId,
                units: raw.length !== 0 ? units.currentUnit : null,
                region,
                pdv: pdv === '1' ? true : pdv === '0' ? false : null,
            };

            console.log('filters connect', val);
            const sliceFalseValues = sliceFalseValuesFromList(val);

            if (socket) {
                if (Object.keys(sliceFalseValues).length !== 0) {
                    handleSocket(sliceFalseValues);
                } else {
                    // console.log('filter rebder', raw);
                    handleSocket(null, 'reset');
                }
            }
        },
        [raw, units.currentUnit, region, pdv],
    );

    const handleUnitsChange = e => {
        setUnit({ ...units, currentUnit: e });
    };

    const handleFiltersChange = async e => {
        setRaw(e);
        if (e.length > 0) {
            const { data } = await api.getUnits(e[2]);
            // console.log('units ', data);
            setUnit({ currentUnit: null, unitsOptions: data });
        }
    };
    return (
        <div className="exchange-filters-block">
            <div className="size-container">
                <div className="form-item">
                    <label htmlFor="">Сировина</label>
                    <Cascader
                        options={products}
                        fieldNames={{
                            label: 'name',
                            value: 'id',
                            children: 'children',
                        }}
                        onChange={handleFiltersChange}
                        placeholder="обрати"
                        displayRender={label => label.join(', ')}
                    />
                </div>
                <CustomSelect
                    options={regions}
                    placeholder="обрати"
                    value={region || undefined}
                    allowClear
                    label="Локація"
                    handleChange={val => setRegion(val)}
                />

                <CustomSelect
                    options={units.unitsOptions}
                    handleChange={handleUnitsChange}
                    label="Одиниця виміру / Вид фасування"
                    placeholder="обрати"
                    allowClear
                    value={units.currentUnit || undefined}
                    disabled={raw.length < 1}
                    tooltipTitle="Необхідно обрати сировину!"
                />

                <CustomSelect
                    options={moneyForm}
                    placeholder="обрати"
                    value={pdv || undefined}
                    allowClear
                    handleChange={val => console.log('a', val) || setPdv(val)}
                    label="Форма розрахунку"
                />
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    products: state.products,
    socket: state.socket,
});
const mapDispatchToProps = dispatch => ({
    fetchForProducts: () => dispatch(fetchAllProductsData()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Filters);

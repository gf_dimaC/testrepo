import React from 'react';
import ConfirmBuyOrder from './ConfirmBuyOrder';
import ConfirmSellOrder from './ConfirmSellOrder';
import { notification } from 'antd';
import { connect } from 'react-redux';
import { deliveryService } from '../../../../../helpers/deliveryService';

const ConfirmOrder = ({
    delivery,
    modalType,
    step,
    nextStep,
    close,
    currentChoosedOrder,
    socket,
    commission,
    user,
}) => {
    const confirmOrder = (id, orderUserEmail, isOpen, deliverPrice = 0) => {
        if (user.userEmail === orderUserEmail) {
            notification.warning({
                message: 'Ви не можете прийняти свою пропозицію!',
            });
            return;
        } else if (isOpen) {
            notification.warning({
                message: 'Цей ордер вже був зерезервованний!',
            });
            return;
        } else if (
            user.verificationStatus === 'N' ||
            !user.verificationStatus
        ) {
            notification.warning({
                message:
                    'Для прийнятя заказу необхідно веріфікувати ваш аккаунт!',
            });
            return;
        }

        socket.send(
            JSON.stringify({
                command: 'open',
                action: 'subscribe',
                message: {
                    order_id: id,
                    deliveryPrice: deliverPrice,
                },
            }),
        );
    };
    const deliveryPrice = deliveryService(currentChoosedOrder, delivery);
    // console.log('xsxsx');
    const unitsName = currentChoosedOrder.applicationData.units.shortName;
    if (modalType === 'buy-confirm') {
        return (
            <ConfirmBuyOrder
                step={step}
                nextStep={nextStep}
                confirmOrder={confirmOrder}
                close={close}
                data={currentChoosedOrder}
                unitsName={unitsName}
                deliveryprice={deliveryPrice}
                user={user}
            />
        );
    } else if (modalType === 'sell-confirm') {
        return (
            <ConfirmSellOrder
                step={step}
                nextStep={nextStep}
                confirmOrder={confirmOrder}
                close={close}
                data={currentChoosedOrder}
                unitsName={unitsName}
                deliveryprice={deliveryPrice}
                commission={commission}
                user={user}
            />
        );
    }
};

const mapStateToProps = state => ({
    currentChoosedOrder: state.currentChoosedOrder,
    delivery: state.delivery,
    socket: state.socket,
    commission: state.commission.amount,
    user: state.user,
});

export default connect(mapStateToProps)(ConfirmOrder);

import React, { useState, useEffect } from 'react';
import { notification } from 'antd';
import { connect } from 'react-redux';
import api from '../../../../../sevices/exchange/index';
import CreateBuyOrder from './CreateBuyOrder';
import CreateSellOrder from './CreateSellOrder';
import { tooglePreloader } from '../../../../../store/actions/preloader';
import userApi from "../../../../../sevices/userRequests"

const errorMap = {
    unitId: 'Заповніть поле з одиницями виміру товару!',
    rawId: 'Заповніть поле з сировиною товару!',
    categoryId: 'Заповніть поле з категорією товару!',
    productId: 'Заповніть поле з назвою товару!',
    volume: 'Заповніть поле з обьемом товару!',
    price: 'Заповніть поле з ціною за товар!',
    // deliveryPrice: 'Заповніть поле з ціною доставки!',
};

const CreateOrder = ({
    onCreateOrder,
    type,
    products,
    socket,
    commission,
                         isOpen,
                         useBalance,
    stock,
    userVerified,
    tooglePreloader,
    loading,
}) => {
    const [order, setOrder] = useState({
            unitId: '',
            rawId: '',
            categoryId: '',
            productId: '',
            volume: null,
            price: null,
            pdv: false,
            deliveryPrice: null,
        }),
        [photo, setPhoto] = useState(''),
        [cert, setCert] = useState(''),
        [certObjForDisplayName, setCertObj] = useState(null),
        [units, setUnits] = useState([]),
        [balance, setBalance] = useState(useBalance);

    useEffect(() => {
        if (isOpen) {
            userApi.getUserBalance().then(({data:{balance}}) => setBalance(balance));
        }

    },[isOpen]);
    const onDrop = (acceptedFiles, type) => {
        const reader = new FileReader();
        if (type === 'cert') {
            setCertObj(acceptedFiles[0]);
        }
        reader.onloadend = function() {
            if (type === 'photo') {
                setPhoto(reader.result);
            } else {
                setCert(reader.result);
            }
        };
        reader.readAsDataURL(acceptedFiles[0]);
    };
    const validate = () => {
        let err = {};
        Object.entries(order).forEach(item => {
            if (!item[1] && item[1] !== false && item[0] !== 'deliveryPrice') {
                err[item[0]] = errorMap[item[0]];
            }
        });
        return err;
    };
    const handleCreateOrder = (data, photo, cert, type) => {
        tooglePreloader(true);
        const transactionMap = {
            buy: {
                type: 'B',
                depth: '1',
            },
            sell: {
                type: 'S',
                depth: '2',
            },
        };

        const sendingData = {
            command: 'send',
            depth: transactionMap[type].depth,
            message: {
                application_data: {
                    applicationType: transactionMap[type].type,
                    price: data.price,
                    certificate: cert,
                    product_image: photo,
                    pdv: data.pdv,
                    deliveryPrice: data.deliveryPrice || 0,
                    applicationItem: {
                        product: data.productId,
                        rawMaterial: data.rawId,
                        category: data.categoryId,
                        purchaseVolume: +data.volume,
                        units: data.unitId,
                    },
                },
            },
        };
        socket.send(JSON.stringify(sendingData));
    };
    const handleChangeInput = (name, value) => {
        switch (name) {
            case 'productId':
                return setOrder({
                    ...order,
                    rawId: '',
                    unitId: '',
                    categoryId: '',
                    [name]: value,
                });
            case 'rawId':
                return setOrder({
                    ...order,
                    categoryId: '',
                    [name]: value,
                });
            case 'categoryId':
                api.getUnits(value).then(({ data }) => setUnits(data));
                return setOrder({
                    ...order,
                    unitId: '',
                    [name]: value,
                });

            default:
                return setOrder({
                    ...order,
                    [name]: value,
                });
        }
    };

    const handleCreate = e => {
        e.preventDefault();
        const isValidated = validate();

        if (Object.keys(isValidated).length > 0) {
            Object.values(isValidated).forEach(err => {
                notification.warning({
                    message: err,
                });
            });
        } else if (!stock) {
            notification.warning({
                title: 'Помилка веріфікацій складу',
                message:
                    'Необхідно веріфікувати склад у вашому профілі для створення заказів на біржі!',
            });
        } else if (userVerified === 'N' || !userVerified) {
            notification.warning({
                message:
                    'Для прийнятя заказу необхідно веріфікувати ваш аккаунт!',
            });
            return;
        } else {
            handleCreateOrder(order, photo, cert, type);
            setOrder({
                unitId: '',
                rawId: '',
                categoryId: '',
                productId: '',
                volume: 0,
                price: 0,
                pdv: false,
            });
            setPhoto({});
            setCert({});
        }
    };

    const selectedProduct = products.find(({ id }) => id === order.productId);
    const selectedMaterial = Boolean(order.rawId)
        ? selectedProduct.children.find(({ id }) => id === +order.rawId)
        : null;
    const categories = selectedMaterial ? selectedMaterial.children : null;
    let sum = +order.volume * +order.price * (+commission / 100);
    let minUnitChoice = order.unitId
        ? units.find(unit => unit.id === order.unitId)
        : '';
    return type === 'buy' ? (
        <CreateBuyOrder
            selectedProduct={selectedProduct}
            order={order}
            categories={categories}
            products={products}
            handleCreate={handleCreate}
            handleChangeInput={handleChangeInput}
            units={units}
            sum={sum}
            minUnitChoice={minUnitChoice}
            balance={balance}
            loading={loading}
            // minUnit={minUnit}
            // setMinUnit={setMinUnit}
        />
    ) : (
        <CreateSellOrder
            loading={loading}
            order={order}
            products={products}
            selectedProduct={selectedProduct}
            handleCreate={handleCreate}
            handleChangeInput={handleChangeInput}
            units={units}
            photo={photo}
            cert={cert}
            onDrop={onDrop}
            categories={categories}
            balance={balance}
            sum={sum}
            minUnitChoice={minUnitChoice}
            certObjForDisplayName={certObjForDisplayName}
        />
    );
};

// const balance = state => +state.user.balance - +state.user.frozenBalance;

const mapStateToProps = state => ({
    useBalance: state.user.balance,
    stock: state.user.stock,
    userVerified: state.user.verificationStatus,
    products: state.products,
    socket: state.socket,
    commission: state.commission.amount,
    loading: state.preloader,
});

const mapDispatchToProps = dispatch => ({
    tooglePreloader: payload => dispatch(tooglePreloader(payload)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CreateOrder);

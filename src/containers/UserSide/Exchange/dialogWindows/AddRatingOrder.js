import React, { useState, useEffect } from 'react';
import { Rate, Icon, notification } from 'antd';
import { fetchRating } from '../../../../store/actions/Users/commission';
import { connect } from 'react-redux';
import api from '../../../../sevices/exchange/index';
import { transformIncomeRatingtoStateStracture } from '../../../../helpers/functions';

const desc = ['Дуже погано', 'Погано', 'Середнє', 'Добре', 'Дуже добре'];

const AddRatingOrder = ({
    acceptActiveOrder,
    currentChoosedOrder,
    rating,
    getRatingData,
    closeModal,
}) => {
    const [ratings, setRating] = useState({});
    // const [canSendRatin]
    const handleChange = (value, id) => {
        setRating({ ...ratings, [id]: value });
    };
    useEffect(() => {
        getRatingData(currentChoosedOrder.id);
    }, []);

    useEffect(
        () => {
            const transformRatingToObj = transformIncomeRatingtoStateStracture(
                rating,
            );

            setRating(transformRatingToObj);
        },
        [rating],
    );

    const sendRating = async () => {
        const data = {
            order: currentChoosedOrder.id,
            questions: Object.entries(ratings)
                .map(rat => ({ id: rat[0], rating: rat[1] }))
                .filter(rat => rat.rating !== 0),
        };
        // console.log(data)
        api.createRatingToOrder(data)
            .then(res => {
                notification.success({
                    message: 'Дякуємо за ваш відгук!',
                });
                closeModal();
                acceptActiveOrder();
                // setRating({})
            })
            .catch(err => {
                notification.error({
                    message: 'Рейтинг тимчасово не доступний!',
                });
                closeModal();
                acceptActiveOrder();
            });
    };

    return (
        <div className="rating-order">
            <div className="window-title">
                Поставте оцінку вашої останньої операції
            </div>

            <div className="description-order">
                <div className="name-order">
                    <span className="name">
                        {`${
                            currentChoosedOrder.applicationData.product.name
                        }  ${
                            currentChoosedOrder.applicationData.category.name
                        }` || 'Не вказано'}
                    </span>
                </div>

                <div style={{ display: 'flex' }}>
                    <div className="description">
                        <div className="price">
                            <label className="title">Ціна:</label>
                            <span className="value">
                                {currentChoosedOrder.price} грн/
                                {
                                    currentChoosedOrder.applicationData.units
                                        .shortName
                                }
                            </span>
                        </div>
                        <div className="count">
                            <label className="title">Кількість:</label>
                            <span className="value">
                                {currentChoosedOrder.applicationData
                                    .purchaseVolume +
                                    currentChoosedOrder.applicationData.units
                                        .shortName}
                            </span>
                        </div>
                    </div>

                    <div className="total">
                        <label className="title">Сума замовлення</label>
                        <div className="value">
                            {currentChoosedOrder.sum} грн
                        </div>
                    </div>
                </div>
            </div>
            {rating.map(({ title, id }) => (
                <div className="rate-block" key={id}>
                    <div>{title}</div>

                    <Rate
                        value={ratings[id]}
                        tooltips={desc}
                        onChange={e => handleChange(e, id)}
                        character={<Icon type="star" />}
                        defaultValue={0}
                    />
                    {ratings[id] ? (
                        <span className="ant-rate-text">
                            {desc[ratings[id] - 1]}
                        </span>
                    ) : (
                        ''
                    )}
                </div>
            ))}

            <button
                className="btn"
                disabled={!Object.values(ratings).find(el => el > 0)}
                onClick={sendRating}
            >
                Оцінити
            </button>
        </div>
    );
};

const mapStateToProps = state => ({ rating: state.rating });
const mapDispatchToProps = dispatch => ({
    getRatingData: id => dispatch(fetchRating(id)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(AddRatingOrder);

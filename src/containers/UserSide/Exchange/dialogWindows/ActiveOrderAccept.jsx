import React, { useState } from 'react';
import { Icon, Modal } from 'antd';
import star from '../../../../assets/img/star.svg';
import { connect } from 'react-redux';
import AddRatingOrder from './AddRatingOrder';
import { BASE_IMG_URL } from '../../../../constants/APIURLS';
import regions from '../../../../constants/regions';
import api from '../../../../sevices/exchange/index';

const ActiveOrderAccept = ({ currentChoosedOrder, socket }) => {
    const [isOpen, toogleModal] = useState(false);
    const [sertNotify, setNotify] = useState('');
    const closeModal = () => {
        toogleModal(false);
    };
    const acceptActiveOrder = () => {
        socket &&
            socket.send(
                JSON.stringify({
                    command: 'close_order',
                    message: {
                        orderId: currentChoosedOrder.id,
                    },
                }),
            );
    };

    const donwloadSertificate = id => () => {
        api.downloadSertificate(id).then(el => console.log(el));
    };
    const downloadContract = () => {
        api.downloadContract().then(el => console.log(el));
    };
    const currentRegion = regions.find(
        el => el.id === currentChoosedOrder.stock.stockRegion,
    );
    const address = `${currentRegion.name}, ${currentChoosedOrder.stock.stockCity}, ${currentChoosedOrder.stock.stockStreet}, ${currentChoosedOrder.stock.stockBuilding}`;
    return (
        <div className="confirm-buy-order">
            <div className="window-title">
                {currentChoosedOrder.applicationData.category.name}
                {'  '}
                {currentChoosedOrder.applicationData.product.name}
            </div>

            <div className="description-order">
                <span className="position">
                    <Icon type="environment" />
                    {address}
                </span>

                <span className="rating">
                    <img src={star} alt="" />
                    {currentChoosedOrder.userRating
                        ? currentChoosedOrder.userRating
                        : '5'}
                </span>
            </div>

            <div className="order-information">
                <div className="item">
                    <div className="title">Ціна:</div>

                    <div className="value">{currentChoosedOrder.price} грн</div>
                </div>
                <div className="item">
                    <div className="title">Обсяг:</div>

                    <div className="value">
                        {currentChoosedOrder.applicationData.purchaseVolume}{' '}
                        {currentChoosedOrder.applicationData.units.shortName}
                    </div>
                </div>
                <div className="item">
                    <div className="title">Товар:</div>

                    <div className="value">
                        {currentChoosedOrder.applicationData.product.name}
                    </div>
                </div>
                <div className="item">
                    <div className="title">Сировина:</div>

                    <div className="value">
                        {currentChoosedOrder.applicationData.rawMaterial.name}
                    </div>
                </div>
                <div className="item">
                    <div className="title">Одиниця виміру:</div>

                    <div className="value">
                        {currentChoosedOrder.applicationData.units.name}
                    </div>
                </div>
                <div className="item">
                    <div className="title">Відстань:</div>

                    <div className="value">
                        {currentChoosedOrder.distance.text || '-'}
                    </div>
                </div>
                <div className="item">
                    <div className="title">Орієнтовна вартість дотсавки:</div>

                    <div className="value">
                        {currentChoosedOrder.deliveryPrice || 0} грн
                    </div>
                </div>
            </div>

            <div className="price">
                {currentChoosedOrder.applicationData.sum}
            </div>

            <div
                className={`download-block ${
                    currentChoosedOrder.certificate ? '' : 'red'
                }`}
            >
                <Icon type="file-pdf" />
                {currentChoosedOrder.haveCertificate ? (
                    <p onClick={donwloadSertificate(currentChoosedOrder.id)}>
                        {' '}
                        Завантажити сертифікат
                    </p>
                ) : (
                    <p> Сертифікат відсутній</p>
                )}
            </div>

            <div className="download-block">
                <Icon type="file-pdf" />
                <p
                    onClick={downloadContract}
                    // download
                    // href="https://cdn.vox-cdn.com/thumbor/Pkmq1nm3skO0-j693JTMd7RL0Zk=/0x0:2012x1341/1200x800/filters:focal(0x0:2012x1341)/cdn.vox-cdn.com/uploads/chorus_image/image/47070706/google2.0.0.jpg"
                >
                    Завантажити договір
                </p>
            </div>

            <div className="confirm-btn">
                {/* <button
                    className="btn yellow"
                    onClick={() => toogleModal(true)}
                >
                    Оцінити
                </button> */}
                <button className="btn" onClick={() => toogleModal(true)}>
                    Завершити <Icon type="arrow-right" />
                </button>
            </div>
            <Modal footer={null} visible={isOpen} onCancel={closeModal}>
                <AddRatingOrder
                    currentChoosedOrder={currentChoosedOrder}
                    closeModal={closeModal}
                    acceptActiveOrder={acceptActiveOrder}
                />
            </Modal>
        </div>
    );
};

const mapStateToProps = state => ({
    currentChoosedOrder: state.currentChoosedOrder,
    socket: state.socket,
});

export default connect(mapStateToProps)(ActiveOrderAccept);

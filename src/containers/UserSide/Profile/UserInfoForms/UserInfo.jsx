import React from 'react';
import { withFormik } from 'formik';
import PersonalInfo from './PersonalInfo';
import { removeNulls } from '../../../../helpers/functions';
import Company from './Company';
import Billing from './Billing';
import Warehouse from './Warehouse';
import { notification } from 'antd';

const UserForm = ({
    values,
    handleChange,
    setFieldValue,
    handleSubmit,
    user,
    updateProfile,
}) => {
    return (
        <form
            className="user-information-block section"
            onSubmit={handleSubmit}
        >
            <PersonalInfo
                user={user}
                values={values}
                setFieldValue={setFieldValue}
                handleChange={handleChange}
            />
            <Company values={values} setFieldValue={setFieldValue} />
            <div className="billing-inf">
                <Billing values={values} setFieldValue={setFieldValue} />
                <Warehouse
                    values={values}
                    updateProfile={updateProfile}
                    setFieldValue={setFieldValue}
                />
                <button
                    className="btn profile_btn"
                    type="submit"
                    disabled={user.loading}
                >
                    Оновити дані
                </button>
            </div>
        </form>
    );
};

const UserInfo = withFormik({
    enableReinitialize: true,
    mapPropsToValues: ({ user }) => ({
        firstName: user.firstName,
        lastName: user.lastName,
        phone: user.phone,
        nin: user.nin,
        organization: user.organization,
        account: user.account,
        stock: user.stock,
        role: user.role,
        email: user.email,
        userRole: user.userRole,
    }),
    handleSubmit: (values, { props: { updateProfile } }) => {
        let err = {};
        console.log(values);
        if (values.nin && values.nin.length >= 10) {
            err.nin = 'ИНН повинен бути не довше довжиною  символів!';
        }
        if (
            values.organization &&
            values.organization.organizationIndex &&
            values.organization.organizationIndex.length !== 5
        ) {
            err.ind = 'Невірний формат індексу компаії';
        }
        let errErr = Object.keys(err);
        if (errErr.length > 0) {
            errErr.forEach(el => {
                notification.error({
                    message: err[el],
                });
            });
            return;
        }

        removeNulls(values);

        updateProfile(values);
    },

    displayName: 'ProfileForm',
})(UserForm);
export default UserInfo;

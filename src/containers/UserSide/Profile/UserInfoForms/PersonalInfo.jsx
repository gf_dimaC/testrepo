import React from 'react';
import CustomSelect from '../../../../components/CustomSelect';
import Input from '../../../../components/Input';
import userTypes from '../../../../constants/userRoles';

const PersonalInfo = ({ values, setFieldValue, handleChange, user }) => {
    return (
        <div className="personal-inf">
            <div className="block-title">Персональна інформація</div>
            <CustomSelect
                options={userTypes}
                label="Роль"
                value={values.userRole}
                handleChange={type => setFieldValue('userRole', type)}
            />

            <Input
                label="Ім'я"
                handleChange={handleChange}
                value={values.firstName}
                name="firstName"
                placeholder="-"
            />
            <Input
                label="Прізвище"
                handleChange={handleChange}
                value={values.lastName}
                name="lastName"
                placeholder="-"
            />
            <Input
                label="Email"
                type="email"
                value={values.email}
                name="email"
                placeholder="-"
                handleChange={handleChange}
            />
            <Input
                label="Телефон"
                type="phone"
                value={values.phone}
                name="phone"
                placeholder="Наприклад: +380982992323 або 80982923232"
                handleChange={handleChange}
            />
            <Input
                label="ІНН"
                value={values.nin}
                name="nin"
                placeholder="-"
                handleChange={handleChange}
            />
            <Input
                label="Країна"
                value={user.userCountry}
                name="nin"
                placeholder="-"
                disabled
                handleChange={handleChange}
            />
        </div>
    );
};

export default PersonalInfo;

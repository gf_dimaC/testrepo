import React from 'react';
import CustomSelect from '../../../../components/CustomSelect';
import Input from '../../../../components/Input';
import {
    organizationMap,
    orgLeader,
} from '../../../../constants/organizationtypes';

const Company = ({ values, setFieldValue }) => {
    return (
        <div className="legal-inf">
            <div className="block-title">Юридичні дані</div>
            <CustomSelect
                options={organizationMap}
                label="Організаційно-правова форма підприємства"
                value={
                    values.organization
                        ? values.organization.organizationType
                        : ''
                }
                handleChange={type =>
                    setFieldValue('organization.organizationType', type)
                }
            />

            <Input
                label="Назва організація"
                value={values.organization ? values.organization.name : ''}
                name="name"
                placeholder="-"
                handleChange={e =>
                    setFieldValue('organization.name', e.target.value)
                }
            />
            <Input
                label="Посвідчення платника ПДВ"
                value={values.organization ? values.organization.pdv : ''}
                name="pdv"
                placeholder="-"
                handleChange={e =>
                    setFieldValue('organization.pdv', e.target.value)
                }
            />
            <Input
                label="ПІБ керуючого"
                value={
                    values.organization ? values.organization.headPosition : ''
                }
                placeholder="-"
                name="headPosition"
                handleChange={e =>
                    setFieldValue('organization.headPosition', e.target.value)
                }
            />
            <CustomSelect
                options={orgLeader}
                label="Посада керівника"
                value={
                    values.organization
                        ? values.organization.aboutOrganization || '1'
                        : '1'
                }
                handleChange={type =>
                    setFieldValue('organization.aboutOrganization', type)
                }
            />

            <Input
                label="Місто, область, країна"
                value={
                    values.organization
                        ? values.organization.organizationCountry
                        : ''
                }
                placeholder="-"
                name="organizationCountry"
                handleChange={e =>
                    setFieldValue(
                        'organization.organizationCountry',
                        e.target.value,
                    )
                }
            />
            <Input
                label="Район"
                value={
                    values.organization
                        ? values.organization.organizationRegion
                        : ''
                }
                placeholder="-"
                name="organizationRegion"
                handleChange={e =>
                    setFieldValue(
                        'organization.organizationRegion',
                        e.target.value,
                    )
                }
            />
            <Input
                label="Вулиця"
                value={
                    values.organization
                        ? values.organization.organizationStreet
                        : ''
                }
                placeholder="-"
                name="organizationStreet"
                handleChange={e =>
                    setFieldValue(
                        'organization.organizationStreet',
                        e.target.value,
                    )
                }
            />

            <Input
                label="Індекс"
                value={
                    values.organization
                        ? values.organization.organizationIndex
                        : ''
                }
                placeholder="-"
                name="organizationIndex"
                handleChange={e =>
                    setFieldValue(
                        'organization.organizationIndex',
                        e.target.value,
                    )
                }
            />
        </div>
    );
};

export default Company;

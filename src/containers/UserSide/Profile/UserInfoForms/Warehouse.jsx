import React, { useState, useEffect, Fragment } from 'react';
import regions from '../../../../constants/regions';
import CustomSelect from '../../../../components/CustomSelect';
import Input from '../../../../components/Input';
import { notification } from 'antd';
import { validateWarehouse } from '../../../../helpers/functions';
import { verifyWarehouse } from '../../../../store/actions/user';
import { connect } from 'react-redux';

const headStyle = { marginTop: '40px' };

const Warehouse = ({ values, updateProfile, fetchForWarehouses }) => {
    const [warehouseData, setWarehouseData] = useState({});

    useEffect(() => {
        setWarehouseData({
            stockCity: values.stock ? values.stock.stockCity : '',
            stockRegion: values.stock ? values.stock.stockRegion : '',
            stockCountry: values.stock ? values.stock.stockCountry : '',
            stockStreet: values.stock ? values.stock.stockStreet : '',
            stockBuilding: values.stock ? values.stock.stockBuilding : '',
        });
    }, [values.stock]);

    const stockVerify = () => {
        if (validateWarehouse(warehouseData))
            return notification.error({
                message: 'Для веріфікації складу необхідно заповнити всі поля!',
            });
        fetchForWarehouses(warehouseData);
    };

    const handleSelect = id => {
        setWarehouseData({
            ...warehouseData,
            stockRegion: id,
        });
    };
    const handleInputs = e => {
        setWarehouseData({
            ...warehouseData,
            [e.target.name]: e.target.value,
        });
    };
    return (
        <Fragment>
            <div className="block-title" style={headStyle}>
                Локація складу
            </div>
            <Input
                placeholder="-"
                label="Місто"
                value={warehouseData.stockCity}
                name="stockCity"
                handleChange={handleInputs}
            />
            <CustomSelect
                placeholder="Область"
                options={regions}
                label="Область"
                value={warehouseData.stockRegion}
                handleChange={handleSelect}
            />

            <Input
                placeholder="-"
                label="Країна"
                value={warehouseData.stockCountry}
                name="stockCountry"
                handleChange={handleInputs}
            />
            <Input
                label="Вулиця"
                placeholder="-"
                value={warehouseData.stockStreet}
                name="stockStreet"
                handleChange={handleInputs}
            />
            <Input
                label="Будинок"
                placeholder="-"
                value={warehouseData.stockBuilding}
                name="stockBuilding"
                handleChange={handleInputs}
            />
            {values.stock && values.stock.isVerified ? (
                <div className="verification-status">Веріфіковано</div>
            ) : (
                <div className="verification-status notVerified">
                    Неверіфіковано
                </div>
            )}
            <button
                className="btn verify_btn"
                type="button"
                onClick={() => stockVerify(values)}
            >
                Веріфікувати Склад
            </button>
        </Fragment>
    );
};

const mapDispatchToProps = dispatch => ({
    fetchForWarehouses: data => dispatch(verifyWarehouse(data)),
});

export default connect(null, mapDispatchToProps)(Warehouse);

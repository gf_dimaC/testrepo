import React, { Fragment } from 'react';
import Input from '../../../../components/Input';

const Billing = ({ values, setFieldValue }) => {
    return (
        <Fragment>
            <div className="block-title">Платіжна інформація</div>
            <Input
                label="Назва банку"
                value={values.account ? values.account.bank : ''}
                name="bank"
                placeholder="-"
                handleChange={e =>
                    setFieldValue('account.bank', e.target.value)
                }
            />
            <Input
                label="МФО"
                value={values.account ? values.account.sortCode : ''}
                name="sortCode"
                placeholder="-"
                handleChange={e =>
                    setFieldValue('account.sortCode', e.target.value)
                }
            />
            <Input
                label="Розрахунковий рахунок"
                value={values.account ? values.account.currentAccount : ''}
                name="currentAccount"
                placeholder="-"
                handleChange={e =>
                    setFieldValue('account.currentAccount', e.target.value)
                }
            />
        </Fragment>
    );
};

export default Billing;

import React from 'react';
import icon from '../../../assets/img/padlock.svg';
import { NavLink } from 'react-router-dom';

const ChangePassword = () => {
    return (
        <div className="change-password-page section">
            <img src={icon} alt="icon#2" />
            <span>Змінити пароль</span>
            <button className="btn">
                <NavLink to="/change_password">Змінити пароль</NavLink>
            </button>
        </div>
    );
};

export default ChangePassword;

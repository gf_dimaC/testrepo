import React from 'react';
import { Table } from 'antd';
import avatar from '../../../assets/img/avatar.svg';
import star from '../../../assets/img/star.svg';
import columns from '../../../helpers/columns';

const verStyle = {
    margin: '0 0 0 20px',
};

const ShortInformation = ({ user, onLogout }) => {
    const mapNewDataArray = user.authHistory
        .map(el => ({
            twoFactorAuthEnabled: el.user2f ? 'Так' : 'Ні',
            userCountry: el.userLocation,
            loginIp: el.loginIp,
            loginDatetime: el.loginDatetime,
        }))
        .sort((a, b) => new Date(b.loginDatetime) - new Date(a.loginDatetime));

    return (
        <div className="short-information-block section">
            <div className="user-info">
                <div className="user-avatar">
                    <img src={avatar} alt="" />
                    <span className="rating">
                        <img src={star} alt="" />
                        4.84
                    </span>
                </div>

                <div>
                    <div className="user-status">
                        <span className="user-name">
                            {user.firstName || 'User'}
                        </span>

                        <span className="logout" onClick={onLogout}>
                            Вийти
                        </span>

                        {user.verificationStatus === 'V' ? (
                            <div
                                className="verification-status"
                                style={verStyle}
                            >
                                Веріфиковано
                            </div>
                        ) : (
                            <div
                                className="verification-status notVerified"
                                style={verStyle}
                            >
                                Неверіфіковано
                            </div>
                        )}
                        {/*<button className="btn verification">*/}
                        {/*    Веріфікація*/}
                        {/*</button>*/}
                    </div>

                    <div className="last-login">
                        Остання авторизація:{'  '}{' '}
                        {mapNewDataArray.length &&
                            mapNewDataArray[0].loginDatetime}{' '}
                        &emsp; IP:{' '}
                        {mapNewDataArray.length && mapNewDataArray[0].loginIp}
                    </div>
                </div>
            </div>

            <div className="login-history">
                <div className="block-title">Останні авторизації</div>
                <Table
                    size="small"
                    dataSource={mapNewDataArray}
                    columns={columns.userShortInfoCol}
                    pagination={false}
                    scroll={false}
                />
            </div>
        </div>
    );
};

export default ShortInformation;

import React from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';

const CostsHistory = ({ dataSource }) => {
    return (
        <div className="costs-history">
            <div className="user-table">
                <div className="table-title">Витрати</div>

                <Table
                    scroll={{ x: 600 }}
                    dataSource={dataSource}
                    columns={columns.costsColumns}
                    pagination={false}
                />
            </div>
        </div>
    );
};

export default CostsHistory;

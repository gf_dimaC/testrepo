import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import TradeHistory from '../Exchange/TradeHistory';
import ActiveOrder from '../Exchange/ActiveOrders';
import handleSockets from '../../../helpers/sockets';
import {
    SocketHandler,
    SocketErrorHandler,
} from '../../../store/actions/Users/orders';
import './contracts.scss';

const Contracts = ({
    userId,
    // socket,
    SocketHandler,
    SocketErrorHandler,
    ordersSum,
}) => {
    useEffect(
        () => {
            // getDelivery();
            handleSockets(SocketHandler, SocketErrorHandler, userId);
        },
        [userId],
    );
    console.log(ordersSum);
    return (
        <div className="contracts-page">
            <div className="page-title">Обсяг по активним угодам</div>

            <div className="total-block">
                <div className="total-buy">
                    <span className="title">Купівля</span>
                    <span className="value">
                        <span>{ordersSum.buySum.toFixed(2)}</span> грн
                    </span>
                </div>
                <div className="total-sell">
                    <span className="title">Продаж</span>
                    <span className="value">
                        <span>{ordersSum.sellSum.toFixed(2)}</span> грн
                    </span>
                </div>
            </div>
            <ActiveOrder />
            {/* <ActiveContracts /> */}

            <TradeHistory />
        </div>
    );
};

const getOrdersSum = ({ activeOrders: { activeOrders } }) => {
    return activeOrders.reduce(
        (result, order) => {
            if (order.type === 'Купівля') {
                result.buySum += +order.sum;
            } else {
                result.sellSum += +order.sum;
            }
            return result;
        },
        { buySum: 0, sellSum: 0 },
    );
};

const mapStateToProps = state => ({
    userId: state.user.id,
    socket: state.socket,
    ordersSum: getOrdersSum(state),
});

const mapDispatchToProps = dispatch => ({
    SocketHandler: (val, socket) => dispatch(SocketHandler(val, socket)),
    // getDelivery: () => dispatch(getDelivery()),
    SocketErrorHandler: err => dispatch(SocketErrorHandler(err)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Contracts);

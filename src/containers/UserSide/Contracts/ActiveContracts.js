import React from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';

const ActiveContracts = ({ data, onOrderSuccess, onOrderDecline }) => {
    return (
        <div className="active-contracts">
            <div className="user-table">
                <div className="table-title">Активні угоди</div>

                <Table
                    dataSource={data}
                    columns={columns.activeContractsColumns(
                        onOrderSuccess,
                        onOrderDecline,
                    )}
                    pagination={false}
                />
            </div>
        </div>
    );
};

export default ActiveContracts;

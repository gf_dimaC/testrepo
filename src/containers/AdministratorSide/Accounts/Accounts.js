import React  from 'react';
import './accounts.scss';
import Filters from '../components/Filters/Filters';
import Statistics from './Statistics';
import AccountsList from './AccountsList';


const Accounts = ({ location }) => {


    return (
        <div className="accounts-page">
            <Statistics />

            <Filters location={location} page="accounts" />

            <AccountsList/>
        </div>
    );
};

export default Accounts;

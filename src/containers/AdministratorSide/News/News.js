import React, { Component } from 'react';
import api from '../../../sevices/adminSide/news';
import './news.scss';
import NewsList from './NewsList';
import { notification } from 'antd';
import { parse } from 'path';

class News extends Component {
    state = {
        newsItems: [],
        totalpage: 1,
        loading: false,
    };

    componentDidMount() {
        this.fetchAllNews();
    }
    loading = value => {
        this.setState({ loading: value });
    };
    deleteNews = id => async () => {
        try {
            this.loading(true);
            const { newsItems } = this.state;
            await api.deleteNews(id);
            const newNewsItems = newsItems.filter(item => item.id !== id);
            this.setState({ newsItems: newNewsItems, loading: false });
            notification.success({
                message: 'Новина видалена!',
            });
        } catch (e) {
            console.log(e);
            this.loading(false);
            notification.error({
                message: 'Помилка сервера',
            });
        }
    };
    fetchAllNews = async page => {
        try {
            this.loading(true);
            const { data } = await api.getAllNews(page);

            this.setState({
                totalpage: data.count,
                newsItems: data.results,
                count: data.count,
                loading: false,
            });
        } catch (e) {
            this.loading(false);
            console.log(e);
        }
    };
    handlePagination = val => {
        this.fetchAllNews(val);
    };
    openCreateNewsPage = () => {
        this.props.history.push('/admin/create-new');
    };
    onNewsItemClick = data => () => {
        const location = {
            pathname: `/admin/create-new/${data.id}/`,
            // state: {
            //   newsItem: data
            // }
        };

        this.props.history.push(location);
    };
    render() {
        const { newsItems, count, totalpage, loading } = this.state;
        return (
            <div className="news-page">
                <div className="create-news-block section">
                    <span>Новини</span>
                    <button className="btn" onClick={this.openCreateNewsPage}>
                        Додати
                    </button>
                </div>

                <NewsList
                    onNewsItemClick={this.onNewsItemClick}
                    newsItems={newsItems}
                    count={count}
                    loading={loading}
                    deleteNews={this.deleteNews}
                    handlePagination={this.handlePagination}
                    totalpage={totalpage}
                />
            </div>
        );
    }
}

export default News;

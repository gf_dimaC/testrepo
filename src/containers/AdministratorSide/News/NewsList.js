import React from 'react';
import { Table } from 'antd';

const NewsList = ({
    handlePagination,
    loading,
    newsItems,
    deleteNews,
    onNewsItemClick,
    totalpage,
}) => {
    const columns = [
        {
            title: 'No',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Заголовок',
            dataIndex: 'title',
            key: 'title',
            ellipsis: true,
        },
        {
            title: 'Дата створення',
            dataIndex: 'createdDate',
            key: 'createdDate',
        },
        {
            title: '',
            dataIndex: 'actions',
            key: 'actions',
            // fixed: 'right',
            render: (prop1, data) => (
                <div className="actions">
                    <button
                        className="btn btn-white"
                        onClick={onNewsItemClick(data)}
                    >
                        Редагувати
                    </button>
                    <button
                        className="btn btn-red"
                        onClick={deleteNews(data.id)}
                    >
                        Видалити
                    </button>
                </div>
            ),
        },
    ];
    console.log(loading);
    const paginationConfig = {
        // defaultCurrent: 1,
        defaultPageSize: 5,
        total: totalpage,
        onChange: handlePagination,
    };
    return (
        <div className="admin-table section">
            <Table
                expandedRowRender={(rec) => <div style={{textAlign: "left"}} dangerouslySetInnerHTML={{ __html: rec.text }}/>}
                scroll={{ x: 900 }}
                loading={loading}
                dataSource={newsItems}
                columns={columns}
                pagination={paginationConfig}
            />
        </div>
    );
};

export default NewsList;

import React from 'react';
import { Icon } from 'antd';

import logo from '../../../../assets/img/logo_head.svg';

import './header.scss';

const Header = ({ logout }) => {
    return (
        <div
            style={{
                background: '#fff',
                boxShadow: '0 2px 7px 0 rgba(0, 0, 0, 0.17)',
            }}
        >
            <div className="admin-header admin-size-container">
                <div className="logo">
                    <img src={logo} alt="" />
                </div>
                <div className="user-exit" onClick={logout}>
                    Вийти
                </div>
                <div className="user-avatar">
                    <Icon type="user" />
                </div>
            </div>
        </div>
    );
};

export default Header;

import React, { useState } from 'react';
import { Icon, Select, DatePicker } from 'antd';

const Option = Select.Option,
    { RangePicker } = DatePicker;

const UsersFilters = ({ onChangeFilters, values, clearFilters }) => {
    const [datePicker, setDatepicker] = useState([]);
    return (
        <div className="filters section">
            <div className="form-item search">
                <Icon type="search" />
                <label htmlFor="">IDконтрагента, Ім’я, Email</label>
                <input
                    type="text"
                    value={values.search}
                    onChange={e => onChangeFilters('search', e.target.value)}
                />
            </div>

            <div className="form-item">
                <label htmlFor="">Статус верифікації</label>
                <Select
                    onChange={val =>
                        onChangeFilters('verification_status', val)
                    }
                >
                    <Option value={'N'}>Неверифікований</Option>
                    <Option value={'V'}>Верифікований</Option>
                </Select>
            </div>

            <div className="form-item">
                <label htmlFor="">Статус контрагента</label>
                <Select onChange={val => onChangeFilters('is_active', val)}>
                    <Option value="true">Активний</Option>
                    <Option value="false">Неактивний</Option>
                </Select>
            </div>

            <div className="form-item">
                <label htmlFor="">Дата свторення</label>
                <RangePicker
                    onChange={(
                        moment,
                        [created_date_after, created_date_before],
                    ) => {
                        onChangeFilters('date', {
                            created_date_after,
                            created_date_before,
                        });
                        setDatepicker(moment);
                    }}
                    value={values.date ? datePicker : null}
                    placeholder={false}
                />
            </div>
            <button onClick={clearFilters} className="btn reset">
                Скасувати фільтри
            </button>
        </div>
    );
};
export default UsersFilters;

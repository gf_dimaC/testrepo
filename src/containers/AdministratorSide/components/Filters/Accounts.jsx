import React, { useState } from 'react';
import { Icon, Select, DatePicker } from 'antd';
import CustomSelect from '../../../../components/CustomSelect';
const Option = Select.Option,
    { RangePicker } = DatePicker;
const filters = [ {
    id: 1,
    name: "Несплачені"
},{
    id: 2,
    name: "Сплачені"
},{
    id: 3,
    name: "Прострочені"
}];

const AccountsFilters = ({allFilters, onChangeFilters, clearFilters}) => {
    const [datePicker, setDatepicker] = useState([]);
    return (
        <div className="filters section">
            <div className="form-item search">
                <input type="text" value={allFilters.search} onChange={e => onChangeFilters('search', e.target.value)}/>
                <Icon type="search" />
                <label htmlFor="">
                    ID контрагента або номер рахунку
                </label>
            </div>
            <CustomSelect
                options={filters}
                value={allFilters.status}
                handleChange={val => onChangeFilters('status', val)}
                label="Статус рахунку"
            />
            {/*<div className="form-item">*/}
            {/*    <label htmlFor="">Статус рахунку</label>*/}
            {/*    <Select>*/}
            {/*        <Option value="1">Несплачені</Option>*/}
            {/*        <Option value="2">Сплачені</Option>*/}
            {/*        <Option value="3">Прострочені</Option>*/}
            {/*    </Select>*/}
            {/*</div>*/}

            <div className="form-item">
                <label htmlFor="">Дата виставлення</label>
                <RangePicker
                    onChange={(moment, [created_after, created_before]) => {
                        onChangeFilters('date', {
                            created_after,
                            created_before,
                        });
                        setDatepicker(moment);
                    }}
                    value={allFilters.date ? datePicker : null}
                    placeholder={false}
                />
            </div>
            <button onClick={clearFilters} className="btn reset">
                Скасувати фільтри
            </button>
        </div>
    );
};
export default AccountsFilters;

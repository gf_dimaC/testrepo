import React from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';
// import onChangeFilters from '../../../store/';

const ContractsList = ({ changePage, openContract, data }) => {
    return (
        <div className="admin-table section">
            <div className="title-table">Список угод</div>
            <Table
                dataSource={data.data}
                columns={columns.allContracts}
                pagination={{
                    pageSize: 10,
                    total: data.count,
                    onChange: val => changePage('page', val),
                }}
                scroll={{ x: 900 }}
                onRow={(record, rowIndex) => {
                    return {
                        onClick: () => openContract(record.id), // click row
                    };
                }}
            />
        </div>
    );
};

// const mapDispatchToProps = dispatch => ({
//     onChangeFilters: (field, val) => dispatch(onChangeFilters(field, val)),
// });

export default ContractsList;

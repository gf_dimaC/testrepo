import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    fetchInitialContract,
    updateContract,
} from '../../../../store/actions/Admin/contracts';
import { statuses } from '../../../../helpers/status';
import './contract.scss';
import ShortInformation from './ShortInformation';
import AllInformation from './AllInformation';

const Contract = ({
    contract,
    match: {
        params: { id },
    },
    getContract,
    updateContract,
}) => {
    const [state, setState] = useState({ comment: '', applicationType: '' });
    const status =
        contract && statuses.find(el => el.name === contract.status).id;
    const optionswithDisabling =
        contract &&
        statuses.map(el => ({
            ...el,
            disabled:
                (status === 'A' || status === 'WP' || status === 'WS') &&
                (el.id === 'WP' || el.id === 'WS')
                    ? false
                    : true,
        }));
    useEffect(() => {
        getContract(id);
    }, []);

    useEffect(
        () => {
            if (contract) {
                setState({
                    comment: contract.comment,
                    applicationType: status,
                });
            }
        },
        [contract],
    );

    const handleChange = (field, val) => {
        setState({ ...state, [field]: val });
    };
    const innerUpdateContract = () => {
        if (!state.applicationType) {
            updateContract(contract.id, { comment: state.comment });
        } else {
            updateContract(contract.id, state);
        }
    };

    return contract ? (
        <div className="contract-page">
            <div className="back-link">
                <Link to="/admin/contracts">Угоди</Link>
                <span> > Угода {id}</span>
            </div>

            {contract && (
                <ShortInformation
                    handleChange={handleChange}
                    data={contract}
                    state={state}
                    optionswithDisabling={optionswithDisabling}
                />
            )}

            <AllInformation
                data={contract}
                update={innerUpdateContract}
                handleChange={handleChange}
                state={state}
            />
        </div>
    ) : (
        <div>loading</div>
    );
};

const mapStateToProps = state => ({
    contract: state.contracts.initialContract,
});
const mapDispatchToProps = dispatch => ({
    getContract: id => dispatch(fetchInitialContract(id)),
    updateContract: (id, data) => dispatch(updateContract(id, data)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Contract);

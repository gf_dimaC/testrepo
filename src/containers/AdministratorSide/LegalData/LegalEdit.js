import React, { Component } from 'react';
import api from '../../../sevices/adminSide/legal';
import { notification } from 'antd';

export default class LegalEdit extends Component {
    state = {
        name: '',
        companyType: '',
        organization: '',
        bankName: '',
        settlementAccount: '',
        sortСode: '',
        headType: '',
        headName: '',
        vat: '',
        usreou: '',
    };
    componentDidMount() {
        const { id } = this.props.match.params;

        if (id) {
            this.fetchLegalData(id);
        }
    }
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };
    fetchLegalData = async id => {
        // const { id } = this.props.match.params;
        try {
            const { data } = await api.getOneLegal(id);
            this.setState({
                ...data,
            });
        } catch (e) {
            console.log(e);
        }
    };
    editLegal = async set => {
        const { history } = this.props;
        const { id } = this.props.match.params;
        try {
            await api.editLegal(this.state, id);
            notification.success({
                message: 'Форма відредагована!',
            });
            history.push('/admin/legal_data');
        } catch (e) {
            if (e.response.status === 400) {
                notification.error({
                    message: Object.values(e.response.data)[0],
                });
            }
            console.log(e);
        }
    };
    render() {
        const {
            name,
            companyType,
            organization,
            bankName,
            settlementAccount,
            sortCode,
            headType,
            headName,
            vat,
            usreou,
        } = this.state;
        return (
            <div className="legal-form">
                <div className="legal-title">
                    <h2>Створити організацію</h2>

                    <button className="btn" onClick={this.editLegal}>
                        Зберегти
                    </button>
                </div>
                <div className="legal-input-wrapper">
                    <div className="form-item">
                        <label htmlFor="formname">Назва форми розрахунку</label>
                        <input
                            type="text"
                            id="formname"
                            name="name"
                            value={name}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="companyType">
                            Організаційно правова форма
                        </label>
                        <input
                            type="text"
                            id="companyType"
                            name="companyType"
                            value={companyType}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="organization">Назва Організація</label>
                        <input
                            type="text"
                            id="organization"
                            name="organization"
                            value={organization}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="head_name">ПІБ</label>
                        <input
                            type="text"
                            id="head_name"
                            name="headName"
                            value={headName}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="head_type">Посада керівника</label>
                        <input
                            type="text"
                            id="head_type"
                            value={headType}
                            name="headType"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="bankName">Назва банку</label>
                        <input
                            type="text"
                            id="bankName"
                            name="bankName"
                            value={bankName}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="settlementAccount">
                            Розрахунковий рахунок
                        </label>
                        <input
                            type="text"
                            id="settlementAccount"
                            name="settlementAccount"
                            value={settlementAccount}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="usreou">ЄДЕРПОУ</label>
                        <input
                            type="text"
                            id="usreou"
                            value={usreou}
                            name="usreou"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="sort_code">МФО</label>
                        <input
                            type="text"
                            id="sort_code"
                            name="sortCode"
                            value={sortCode}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="vat">Посвідчення платника ПДВ</label>
                        <input
                            type="text"
                            id="vat"
                            name="vat"
                            value={vat}
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

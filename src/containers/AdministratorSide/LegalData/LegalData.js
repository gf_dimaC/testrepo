import React, { Component } from 'react';
import { Table } from 'antd';
import api from '../../../sevices/adminSide/legal';
import { notification } from 'antd';
import Settings from '../Settings/Settings';
import './legaldata.scss';

class LegalData extends Component {
    state = {
        allLegals: [],
        count: 0,
        loading: false,
    };
    componentDidMount() {
        this.fetchAllLegals();
    }
    loading = value => {
        this.setState({ loading: value });
    };
    fetchAllLegals = async () => {
        try {
            this.loading(true);
            const { data } = await api.getLegals();

            this.setState({
                allLegals: data.results,
                count: data.count,
                loading: false,
            });
        } catch (e) {
            this.loading(false);
            console.log(e);
        }
    };
    handleCreate = () => {
        const { history } = this.props;
        history.push('/admin/legal_data/new');
    };
    handleEdit = id => {
        const { history } = this.props;
        const { allLegals } = this.state;
        const clickedObj = allLegals.find(el => el.id === id);

        history.push(`/admin/legal_data/edit/${clickedObj.id}/`);
    };
    handleDelete = async id => {
        const { allLegals } = this.state;
        try {
            this.loading(true);
            await api.deleteLegal(id);
            const deletedLegals = allLegals.filter(item => item.id !== id);
            this.setState({ allLegals: deletedLegals, loading: false });
            notification.success({
                message: 'Данні видалено!',
            });
        } catch (e) {
            console.log(e);
            this.loading(true);
            notification.error({
                message: 'Помилка сервера',
            });
        }
    };
    render() {
        const columns = [
            {
                title: 'Назва форми розрахунку',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: 'Організаційно правова форма',
                dataIndex: 'companyType',
                key: 'companyType',
            },
            {
                title: 'Організація',
                dataIndex: 'organization',
                key: 'organization',
            },
            {
                title: 'Назва банку',
                dataIndex: 'bankName',
                key: 'bankName',
            },
            {
                title: 'Розрахукновий рахунок',
                dataIndex: 'settlementAccount',
                key: 'settlementAccount',
            },
            {
                title: '',
                dataIndex: 'actions',
                key: 'actions',
                render: (prop, itemdata) => (
                    <div className="actions">
                        <button
                            className="btn btn-white"
                            onClick={() => this.handleEdit(itemdata.id)}
                        >
                            Редагувати
                        </button>
                        <button
                            className="btn btn-red"
                            onClick={() => this.handleDelete(itemdata.id)}
                        >
                            Видалити
                        </button>
                    </div>
                ),
            },
        ];

        const { allLegals, loading } = this.state;
        return (
            <div className="legal-data-page">
                <Settings/>
                <div className="admin-table section">
                    <div className="title-table">
                        Юридичні дані
                        <button className="btn" onClick={this.handleCreate}>
                            Додати
                        </button>
                    </div>
                    <Table
                        scroll={{ x: 900 }}
                        loading={loading}
                        dataSource={allLegals}
                        columns={columns}
                        pagination={false}
                    />
                </div>
            </div>
        );
    }
}

export default LegalData;

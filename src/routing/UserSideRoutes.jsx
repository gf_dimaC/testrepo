import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import UserNews from '../containers/UserSide/News/News';
import WaitingRoute from '../sevices/WaitingRoute';
import UserSide from '../containers/UserSide/UserSide';
import Confidantial from '../components/Confidantial';
import HomePage from '../components/HomePage';
import RegistrationPage from '../components/Registration/RegistrationPage';
import LoginPage from '../components/Login/LoginPage';
import ResetPasswordPage from '../components/ResetPasswordPage';
import ChangePasswordPage from '../components/ChangePasswordPage';
import Exchange from '../containers/UserSide/Exchange/Exchange';
import Profile from '../containers/UserSide/Profile/Profile';
import Contracts from '../containers/UserSide/Contracts/Contracts';
import Balance from '../containers/UserSide/Balance/Balance';
import Orders from '../containers/UserSide/Orders/Orders';
import Accounts from '../containers/UserSide/Accounts/Accounts';
import UsingRights from '../components/UsingRights';

const UserSideRoutes = props => {
    return localStorage.getItem('ADMINTOKEN') ? (
        <Redirect to="/admin/dashboard" />
    ) : (
        <UserSide {...props}>
            <Switch>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/news" component={UserNews} />
                <Route exact path="/support" component={Confidantial} />
                <Route exact path="/UsingRights" component={UsingRights} />
                <Route
                    path="/registration"
                    render={props => {
                        if (localStorage.getItem('TOKEN')) {
                            return <Redirect to="/profile" />;
                        } else if (localStorage.getItem('ADMINTOKEN')) {
                            return <Redirect to="/admin/dashboard" />;
                        } else {
                            return <RegistrationPage {...props} />;
                        }
                    }}
                />
                <Route
                    exact={false}
                    path="/login"
                    render={props => {
                        if (localStorage.getItem('TOKEN')) {
                            return <Redirect to="/profile" />;
                        } else if (localStorage.getItem('ADMINTOKEN')) {
                            return <Redirect to="/admin/dashboard" />;
                        } else {
                            return <LoginPage {...props} />;
                        }
                    }}
                />
                <Route
                    path="/reset_password"
                    render={props => {
                        if (localStorage.getItem('TOKEN')) {
                            return <Redirect to="/profile" />;
                        } else if (localStorage.getItem('ADMINTOKEN')) {
                            return <Redirect to="/admin/dashboard" />;
                        } else {
                            return <ResetPasswordPage {...props} />;
                        }
                    }}
                />

                <WaitingRoute exact path="/profile" component={Profile} />

                <WaitingRoute exact path="/exchange" component={Exchange} />

                <WaitingRoute exact path="/contracts" component={Contracts} />

                <WaitingRoute exact path="/balance" component={Balance} />

                <WaitingRoute exact path="/orders" component={Orders} />
                <WaitingRoute
                    exact
                    path="/change_password"
                    component={ChangePasswordPage}
                />
                <WaitingRoute exact path="/accounts" component={Accounts} />
            </Switch>
        </UserSide>
    );
};
export default UserSideRoutes;

import React, { Fragment } from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

import AdminRoutes from './AdminRoutes';

import UserSideRoutes from './UserSideRoutes';

const Routes = () => {
    return (
        <Fragment>
            <Router>
                <Switch>
                    <Route
                        path="/admin"
                        render={props => <AdminRoutes {...props} />}
                    />
                    <Route
                        path="/"
                        render={props => <UserSideRoutes {...props} />}
                    />
                </Switch>
            </Router>
        </Fragment>
    );
};

// const mapStateToProps = state => {
//     return {
//         user: state.user,
//     };
// };
export default Routes;

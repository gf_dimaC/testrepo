import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import store from './store/store';
import { BrowserRouter as Router } from 'react-router-dom';

// const func = () => {
//     const min = 372037;
//     const max = 905157;
//     const func2 = arr => {
//         let res = true;
//         arr.forEach((el, idx) => {
//             if (+el > +arr[idx + 1]) {
//                 res = false;
//             }
//         });
//         return res;
//     };
//     let result = 0;
//     for (let i = min; i <= max; i++) {
//         const split = String(i).split('');
//         // console.log(split);
//         const res = func2(split);
//         if (res) {
//             result += 1;
//         }
//     }
//     return result;
// };
// console.log(func());
render(
    <Provider store={store}>
        <Router>
            <App />
        </Router>
    </Provider>,
    document.getElementById('root'),
);

import * as Yup from 'yup';

export const LoginSchema = Yup.object().shape({
    password: Yup.string()
        .min(3)
        .required('Введіть пароль!'),

    email: Yup.string()
        .email('Невірний формат email')
        .required('Введіть email!'),
});

export const ResetPasswordSchema = Yup.object().shape({
    captch: Yup.bool().oneOf([true], 'Заповніть captch`у!'),
    email: Yup.string()
        .email('Невірний формат email')
        .required('Введіть email!'),
});

export const RegistrationSchema = Yup.object().shape({
    username: Yup.string()
        .min(3)
        .required('Заповніть поле з вашим ім`ям!'),

    phone: Yup.string()
        .matches(
            /^(\+?\d{2})?-?(\d{2,4})-?(\d{3,4})-?(\d{2,3})-?(\d{2,3})$/,
            'Невірний телефонний номер!',
        )
        .required('Заповніть поле з вашим телефоним номером!'),
    password: Yup.string()
        .min(3)
        .required('Заповніть поле з вашим паролем!'),
    repeatPassword: Yup.string().required('Заповніть поле з вашим паролем!'),
    email: Yup.string()
        .email('Невірний формат email адреси!')
        .required('Заповныть поле email!'),
    permission: Yup.bool().oneOf([true], 'Угода повинна бути прийнята'),
    captch: Yup.bool().oneOf([true], 'Заповніть captch`у!'),
});

export const ChangePasswordSchema = Yup.object().shape({
    oldPassword: Yup.string()
        .min(3)
        .required('Заповніть всі поля!'),

    newPassword: Yup.string()
        .min(3)
        .required('Заповніть всі поля!'),
    confirmPassword: Yup.string()
        .min(3)
        .required('Заповніть всі поля!'),
});
export const updateprofileSchema = Yup.object().shape({
    email: Yup.string().email('Невірний формат email адреси!'),
    // nin: Yup.number().typeError("Невірний формат ІНН")
});
